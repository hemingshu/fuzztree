package OrgID3;

import java.util.ArrayList;

import tools.readOrgData;
import treeTools.TreeNode;

public class testtree {
	private static int resultofData;
	private static double[] resultlist;
	private static double[][] prolist;
	private static int[] listPre;
	private static int[] listReal;
	public static void testModel(readOrgData file,TreeNode root) {
		double[][] data=file.getData();		
		String[] titles=file.getTitles();
		int hangshu=data.length;
		int lieshu=data[0].length;
		int[][] attributes=new int[hangshu][lieshu-1];
		listReal=new int[hangshu];
		for(int i=0;i<hangshu;i++)
		{
			for(int j=0;j<lieshu-1;j++)
				attributes[i][j]=(int) data[i][j];
			listReal[i]=(int) data[i][lieshu-1];
		}
		listPre=new int[data.length];
		int num_features=treeTools.entropy.getFeatures(listReal).size();
		prolist=new double[hangshu][num_features];
		for(int i=0;i<hangshu;i++)
		{
			resultofData=0;
			resultlist=new double[num_features];
			ResultTest(attributes[i],titles,root);
			for(int j=0;j<resultlist.length;j++)
				prolist[i][j]=resultlist[j];
			listPre[i]=resultofData;
		}
	}
	public static int[] getListReal() {
		return listReal;
	}
	public static int[] getListPre() {
		return listPre;
	}
	public static double[][] getProlist(){
		return prolist;
	}
	private static void ResultTest(int[] data,String[] titles,TreeNode node) {
		int res_temp=getlabel(node);
		setprobobilites(node);
		if(res_temp!=-1)			
			resultofData=res_temp;					
		//是叶子节点，则返回
		if(node.getIsleaf()) 			
			return;
		//非叶子节点，继续按数的分类寻找结果
		int index=0;
		for(int i=0;i<titles.length;i++)
		{
			if(node.getName().equals(titles[i]))
			{
				index=i;
				break;
			}
		}
		String[] newtitles=new String[titles.length-1];
		int[] newdata=new int[data.length-1];
		int count=0;
		for(int i=0;i<data.length-1;i++) {
			if(i!=index) {
				newtitles[count]=titles[i];
				newdata[count]=data[i];
				count++;
			}
		}
		newtitles[count]=titles[titles.length-1];
		ArrayList<String> features=node.getFeatures();
		for(int i=0;i<features.size();i++)		
			if(Integer.valueOf(features.get(i))==data[index])
				ResultTest(newdata,newtitles,node.getAttributes().get(i));		
	}
	private static void setprobobilites(TreeNode node) {
		ArrayList<String> label=node.getlabel();
		ArrayList<Double> probobilities=node.getPropobilities();
		for(int i=0;i<resultlist.length;i++)
		{
			boolean flag=true;
			for(int j=0;j<label.size();j++)
				if(i==Integer.valueOf(label.get(j)))
				{
					resultlist[i]=probobilities.get(j);
					flag=false;
				}
			if(flag)
				resultlist[i]=0;						
		}				
	}
	private static int getlabel(TreeNode node) {
		int res=-1;
		ArrayList<Double> probobilities=node.getPropobilities();
		if(probobilities.size()==0)
			return res;
		double maxPro=0.0;
		int maxIndex=0;
		boolean flag=false;
		for(int i=0;i<probobilities.size();i++)
		{
			if(probobilities.get(i)>maxPro)
			{
				maxPro=probobilities.get(i);
				maxIndex=i;
				flag=false;
			}
			else if(probobilities.get(i)==maxPro)			
				flag=true;			
		}
		if(flag)
			return res;
		res=Integer.valueOf(node.getlabel().get(maxIndex));
		return res;
	}
}
