package OrgID3;

import java.util.ArrayList;

import tools.readOrgData;
import treeTools.TreeNode;

public class buildtree {
	private static int times;
	public static void initTimes() {
		times=0;
	}
	public static int getTimes() {
		return times;
	}
	private static boolean once_flag=false;
	public static TreeNode trainModel(readOrgData file) {
		once_flag=false;
		double[][] data=file.getData();		
		String[] titles=file.getTitles();
		int hangshu=data.length;
		int lieshu=data[0].length;
		int[][] attributes=new int[hangshu][lieshu-1];
		int[] label=new int[hangshu];
		for(int i=0;i<hangshu;i++)
		{
			for(int j=0;j<lieshu-1;j++)
				attributes[i][j]=(int) data[i][j];
			label[i]=(int) data[i][lieshu-1];
		}
		TreeNode root=builder(attributes, titles, label);
		return root;
	}
	private static TreeNode builder(int[][] data,String[] titles,int[] label) {
		times++;
		TreeNode node=new TreeNode();
		//所有特征都使用完了or非零数据数量不足时的叶子节点or剩下的特征不能区分结果
		if(titles.length==1 || data.length<tools.Const.getSeta()||IsAttributes(data))
		{
			SetLeaf(node,label,titles[titles.length-1]);
			return node;
		}
		//置信水平较高时得到的叶子节点	
		if(!once_flag)
			once_flag=true;
		else
			if(IsLeaf(node,label,titles[titles.length-1]))
				return node;	
		//找出增益最大的特征作为节点
		double maxGain=0.0;
		int maxIndex=0;
		double[] feaprobobilities=new double[0];
		String[] features=new String[0];
		//
		for(int i=0;i<data[0].length;i++)
		{
			int[] attributes=new int[data.length];
			for(int j=0;j<data.length;j++)
				attributes[j]=data[j][i];
			double temp=treeTools.entropy.getGain(attributes, label);
			//System.out.println(temp);
			if(temp>=maxGain)
			{
				maxGain=temp;
				maxIndex=i;
				ArrayList<Integer> featureSet=treeTools.entropy.getFeatures(attributes);
				feaprobobilities=new double[featureSet.size()];
				features=new String[featureSet.size()];
				feaprobobilities=treeTools.entropy.getprobobility(attributes);
				for(int j=0;j<featureSet.size();j++)
					features[j]=String.valueOf(featureSet.get(j));
			}
		}
		node.setName(titles[maxIndex]);
		node.setIsleaf(false);
		String[] newtitles=new String[titles.length-1];
		int count=0;
		for(int i=0;i<titles.length;i++)		
			if(i!=maxIndex)
			{
				newtitles[count]=titles[i];
				count++;
			}		
		int[] Maxattributes=new int[data.length];
		for(int j=0;j<data.length;j++)
			Maxattributes[j]=data[j][maxIndex];
		for(int i=0;i<features.length;i++)
		{
			node.addFeatures(features[i]);
			node.addFeaProbobilities(feaprobobilities[i]);
			int[] newlabel=treeTools.entropy.getSubCollection(Maxattributes,Integer.valueOf(features[i]), label);
			int[][] newdata=new int[newlabel.length][data[0].length-1];
			int count1=0;
			for(int j=0;j<data[0].length;j++)		
				if(j!=maxIndex)
				{
					int[] subdata=new int[data.length];
					for(int m=0;m<data.length;m++)
						subdata[m]=data[m][j];
					int[] newsubdata=treeTools.entropy.getSubCollection(Maxattributes,Integer.valueOf(features[i]), subdata);
					for(int m=0;m<newsubdata.length;m++)
						newdata[m][count1]=newsubdata[m];
					count1++;
				}
			node.addAttributes(builder(newdata,newtitles,newlabel));			
		}
		return node;
	}
	private static boolean IsAttributes(int[][] data) {
		boolean res=true;
		for(int i=0;i<data[0].length;i++)
		{
			int[] features=new int[data.length];
			for(int j=0;j<data.length;j++)
				features[j]=data[j][i];
			int featureNum=treeTools.entropy.getFeatures(features).size();
			if(featureNum>1)
				res=false;
		}
		return res;
	}
	private static boolean IsLeaf(TreeNode node,int[] label,String name) {
		double[] probobilities=treeTools.entropy.getprobobility(label);
		ArrayList<Integer> features=treeTools.entropy.getFeatures(label);
		int lieshu=probobilities.length;
		for(int i=0;i<lieshu;i++)
		{
			node.addProbobilities(probobilities[i]);
			node.addlabel(String.valueOf(features.get(i)));			
		}
		node.setMaxlabel();
		node.setMaxprobobility();
		if(node.getMaxprobobility()>=tools.Const.getBelt())
		{
			node.setName(name);
			node.setIsleaf(true);
			return true;
		}
		node.setIsleaf(false);		
		return false;
	}
	private static void SetLeaf(TreeNode node,int[] label,String name) {
		double[] probobilities=treeTools.entropy.getprobobility(label);
		ArrayList<Integer> features=treeTools.entropy.getFeatures(label);
		int lieshu=probobilities.length;
		for(int i=0;i<lieshu;i++)
		{
			node.addProbobilities(probobilities[i]);
			node.addlabel(String.valueOf(features.get(i)));			
		}
		node.setMaxlabel();
		node.setMaxprobobility();
		node.setName(name);
		node.setIsleaf(true);
	}
}
