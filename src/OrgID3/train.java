package OrgID3;

import java.io.IOException;

import OrgID3.buildtree;
import tools.readOrgData;
import treeTools.TreeNode;

public class train {
	public static void main(String[] args) throws IOException {
		trainModel();
	}
	public static void trainModel() throws IOException {
		readOrgData file=new readOrgData(tools.Const.getExactTrainfile());	
		//file.printFile();
		TreeNode root=buildtree.trainModel(file);
		//treeTools.Tools.printTree(root,0);
		treeTools.Tools.savemodel(root);
		//System.out.println("code finished");
	}
}
