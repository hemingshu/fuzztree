package testSample;


public class readConfigData {
	String name;
    double[] configlist;
	public readConfigData(String data){
		String[] temp0=data.split(":");
		name=temp0[0];
		String[] temp=temp0[1].split(",");
		configlist=new double[temp.length];
		for(int i=0;i<temp.length;i++)
			configlist[i]=Double.valueOf(temp[i]);
	}
	public String getname(){
		return name;
	}
	public double[] getconfiglist() {
		return configlist;
	}
}
