package testSample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class sample {
	private static String[] modelist;
	private static String[] setlist= {"alpha","belt","seta","RFtreeNum","RFfeatureNum","RFselectTreeRate"};
	private static configerSet set;
	private static String exactrianfile=RandomForest.RFTools.creatfilename(-1, tools.Const.getExactTrainfile());
	private static String exactestfile=RandomForest.RFTools.creatfilename(-1,tools.Const.getExactTestfile());
	private static String fuzztrianfile=RandomForest.RFTools.creatfilename(-1,tools.Const.getFuzzTrainfile());
	private static String  fuzztestfile=RandomForest.RFTools.creatfilename(-1,tools.Const.getFuzzTestfile());
	private static String RFobbfile=RandomForest.RFTools.creatfilename(-1,tools.Const.getRFobbfile());
	private static String modelfile=RandomForest.RFTools.creatfilename(-1,tools.Const.getModelfile());
	
	public static void main(String args[]) throws IOException {
		set=initConfig();
		printSet();
		test();
	}
	private static configerSet initConfig() throws IOException {
		File file = new File(tools.Const.gettestSample());
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(file));
		String s = null;
		s = br.readLine();
		modelist=s.split(",");
		ArrayList<readConfigData> configline=new ArrayList<readConfigData>();
		while ((s = br.readLine()) != null) {
			if(!s.contains("//")) {
				readConfigData configtemp=new readConfigData(s);
				configline.add(configtemp);
			}		
		}
		configerSet res=new configerSet(configline);
		return res;
	}
	private static void test() throws IOException {
		DecimalFormat df = new DecimalFormat( "0.0000");
		File file = new File(tools.Const.getresult());
		FileWriter fw = new FileWriter(file);
		for(int i=0;i<modelist.length;i++)
		{
			initfilepath();
			writeline(fw,"---------------"+modelist[i]+"---------------");
			//读取参数列表
			double[] alphalist=set.getconfig(setlist[0]);
			double[] beltlist=set.getconfig(setlist[1]);
			double[] setalist=set.getconfig(setlist[2]);
			double[] RFtreeNumlist=set.getconfig(setlist[3]);
			double[] RFfeatureNumlist=set.getconfig(setlist[4]);
			double[] RFselectTreeRatelist=set.getconfig(setlist[5]);
			//初始化参数序数
			int[] cfgIdx=new int[setlist.length];
			int[] maxcfgIdx=new int[setlist.length];
			int maxIdx=1;
			for(int j=0;j<maxcfgIdx.length;j++)
			{
				maxcfgIdx[j]=set.getconfig(setlist[j]).length;
				maxIdx=maxIdx*set.getconfig(setlist[j]).length;
			}				
			//判断belt和seta参数是否为空
			if(beltlist==null||setalist==null)
			{
				writeline(fw,"please input beta/seta configer correctly");
				return;
			}
			//ID3训练
			if(modelist[i].equals("ID3"))
			{
				writeNumNodeHead(fw);
				writeline(fw,"beta   seta   ACC");				
				for(int m=0;m<beltlist.length*setalist.length;m++)
				{
					initNumNode();
					System.out.println("--------------------round"+m+"--------------------");
					setIdx(cfgIdx,m,2,maxcfgIdx);
					setConst(cfgIdx);
					OrgID3.train.trainModel();				
					OrgID3.test.testModel(tools.Const.getExactTestfile());
					printNumNode(fw);
					writeline(fw,beltlist[cfgIdx[1]]+"   "+
					        (int)setalist[cfgIdx[2]]+"   "+
					             df.format(OrgID3.test.getACC()));
				}
			}
			//fuzzID3
			if(alphalist==null)
			{
				writeline(fw,"please input alpha configer correctly");
				return;
			}			
			tools.Const.setfuzzSwitch(false);
			if(modelist[i].equals("fuzzID3"))
			{
				writeline(fw,"alpha   beta   seta   ACC");				
				for(int m=0;m<alphalist.length*beltlist.length*setalist.length;m++)
				{
					System.out.println("--------------------round"+m+"--------------------");
					setIdx(cfgIdx,m,3,maxcfgIdx);
					setConst(cfgIdx);
					ID3.train.trainModel();				
					ID3.test.testModel(tools.Const.getFuzzTestfile());
					writeline(fw,alphalist[cfgIdx[0]]+"   "+
					             beltlist[cfgIdx[1]]+"   "+
							(int)setalist[cfgIdx[2]]+"   "+
					             df.format(ID3.test.getACC()));
				}
			}
			//fuzzID3G
			tools.Const.setfuzzSwitch(true);
			if(modelist[i].equals("fuzzID3G"))
			{
				writeline(fw,"alpha   beta   seta   ACC");				
				for(int m=0;m<alphalist.length*beltlist.length*setalist.length;m++)
				{
					System.out.println("--------------------round"+m+"--------------------");
					setIdx(cfgIdx,m,3,maxcfgIdx);
					setConst(cfgIdx);
					ID3.train.trainModel();				
					ID3.test.testModel(tools.Const.getFuzzTestfile());
					writeline(fw,alphalist[cfgIdx[0]]+"   "+
					             beltlist[cfgIdx[1]]+"   "+
							(int)setalist[cfgIdx[2]]+"   "+
					             df.format(ID3.test.getACC()));
				}
			}
			//RFID3
			if(RFtreeNumlist==null||RFfeatureNumlist==null||RFselectTreeRatelist==null)
			{
				writeline(fw,"please input RFtree configer correctly");
				return;
			}			
			if(modelist[i].equals("RFID3"))
			{
				int flag=0;
				writeline(fw,"alpha   beta   seta   RFtreeNum   RFfeatureNum   RFselectTreeRate   aveACC   mostACC   weightACC");				
				for(int m=0;m<maxIdx;m++)
				{
					System.out.println("--------------------round"+m+"--------------------");
					flag=exactRF(m,flag,cfgIdx,maxcfgIdx,RFselectTreeRatelist.length);
					//写入文件
					writeline(fw,alphalist[cfgIdx[0]]+"   "+
					             beltlist[cfgIdx[1]]+"   "+
							(int)setalist[cfgIdx[2]]+"   "+
					        (int)RFtreeNumlist[cfgIdx[3]]+"   "+
					        (int)RFfeatureNumlist[cfgIdx[4]]+"   "+
					        	 RFselectTreeRatelist[cfgIdx[5]]+"   "+
					             df.format(RandomForest.exactTest.getAveACC())+"   "+
					             df.format(RandomForest.exactTest.getMostACC())+"   "+
					             df.format(RandomForest.exactTest.getWightACC()));
				}
			}
			//fuzzRFID3
			tools.Const.setfuzzSwitch(false);
			if(modelist[i].equals("fuzzRFID3"))
			{
				int flag=0;
				writeline(fw,"alpha   beta   seta   RFtreeNum   RFfeatureNum   RFselectTreeRate   aveACC   mostACC   weightACC");				
				for(int m=0;m<maxIdx;m++)
				{
					System.out.println("--------------------round"+m+"--------------------");
					flag=fuzzRF(m,flag,cfgIdx,maxcfgIdx,RFselectTreeRatelist.length);
					//写入文件
					writeline(fw,alphalist[cfgIdx[0]]+"   "+
					             beltlist[cfgIdx[1]]+"   "+
							(int)setalist[cfgIdx[2]]+"   "+
					        (int)RFtreeNumlist[cfgIdx[3]]+"   "+
					        (int)RFfeatureNumlist[cfgIdx[4]]+"   "+
					        	 RFselectTreeRatelist[cfgIdx[5]]+"   "+
					             df.format(RandomForest.fuzztest.getAveACC())+"   "+
					             df.format(RandomForest.fuzztest.getWightACC()));
				}
			}
			//fuzzRFID3G
			tools.Const.setfuzzSwitch(true);
			if(modelist[i].equals("fuzzRFID3G"))
			{
				int flag=0;
				writeline(fw,"alpha   beta   seta   RFtreeNum   RFfeatureNum   RFselectTreeRate   aveACC   mostACC   weightACC");				
				for(int m=0;m<maxIdx;m++)
				{
					System.out.println("--------------------round"+m+"--------------------");
					flag=fuzzRF(m,flag,cfgIdx,maxcfgIdx,RFselectTreeRatelist.length);
					//写入文件
					writeline(fw,alphalist[cfgIdx[0]]+"   "+
					             beltlist[cfgIdx[1]]+"   "+
							(int)setalist[cfgIdx[2]]+"   "+
					        (int)RFtreeNumlist[cfgIdx[3]]+"   "+
					        (int)RFfeatureNumlist[cfgIdx[4]]+"   "+
					        	 RFselectTreeRatelist[cfgIdx[5]]+"   "+
					             df.format(RandomForest.fuzztest.getAveACC())+"   "+
					             df.format(RandomForest.fuzztest.getWightACC()));
				}
			}
		}
		fw.close();
	}
	//初始化要观测的参数
	private static void writeNumNodeHead(FileWriter fw) throws IOException {
		fw.write("times Maxdepth leaf node");					
		fw.flush();
	}
	private static void initNumNode() {
		OrgID3.buildtree.initTimes();
		treeTools.Tools.initMaxdepth();
		treeTools.Tools.initNumleaf();
		treeTools.Tools.initNumnodes();		
	}
	//打印要观测的参数
	private static void printNumNode(FileWriter fw) throws IOException {	
		fw.write(OrgID3.buildtree.getTimes()+" ");
		fw.write(treeTools.Tools.getMaxdepth()+" ");
		fw.write(treeTools.Tools.getNumleaf()+" ");
		fw.write(treeTools.Tools.getNumnode()+" ");
		fw.flush();
	}
	//生成训练用的参数
	private static int fuzzRF(int m,int flag,int[] cfgIdx,int[] maxcfgIdx,int length) throws IOException {
		//初始化参数
		initfilepath();
		//设定参数
		setIdx(cfgIdx,m,6,maxcfgIdx);
		//获得下标
		setConst(cfgIdx);
		//训练测试
		if(flag==length)
			flag=0;
		if(flag==0)
			RandomForest.fuzzTrain.train();
		initfilepath();
		RandomForest.fuzztest.test();
		flag++;
		return flag;
	}
	private static int exactRF(int m,int flag,int[] cfgIdx,int[] maxcfgIdx,int length) throws IOException {
		//初始化参数
		initfilepath();
		//设定参数
		setIdx(cfgIdx,m,6,maxcfgIdx);
		//获得下标
		setConst(cfgIdx);
		//训练测试
		if(flag==length)
			flag=0;
		if(flag==0)
			RandomForest.exactTrain.train();
		initfilepath();
		RandomForest.exactTest.test();
		flag++;
		return flag;
	}
	private static void setIdx(int[] cfgIdx,int m,int idxNum,int[] maxcfgIdx) {
			int[] idx= {1,2,0,3,4,5};
			for(int i=idxNum-1;i>=0;i--)
			{
				cfgIdx[idx[i]]=m%maxcfgIdx[idx[i]];
				m=m/maxcfgIdx[idx[i]];
			}						
	}
	private static void setConst(int[] cfgidx) {
		if(set.getconfig(setlist[0])!=null)
			tools.Const.setAlpha(set.getconfig(setlist[0])[cfgidx[0]]);
		if(set.getconfig(setlist[1])!=null)
			tools.Const.setBelt(set.getconfig(setlist[1])[cfgidx[1]]);
		if(set.getconfig(setlist[2])!=null)
			tools.Const.setSeta((int)set.getconfig(setlist[2])[cfgidx[2]]);
		if(set.getconfig(setlist[3])!=null)
			tools.Const.setRFtreeNum((int)set.getconfig(setlist[3])[cfgidx[3]]);
		if(set.getconfig(setlist[4])!=null)
			tools.Const.setRFfeatureNum((int)set.getconfig(setlist[4])[cfgidx[4]]);
		if(set.getconfig(setlist[5])!=null)
			tools.Const.setRFselectTreeNum(set.getconfig(setlist[5])[cfgidx[5]]);
		
	}
	public static void writeline(FileWriter fw,String s) throws IOException {
		fw.write(s);					
		fw.write("\r\n");
		fw.flush();	
	}
	private static void initfilepath() {
		tools.Const.setExactTrainfile(exactrianfile);
		tools.Const.setExactTestfile(exactestfile);
		tools.Const.setFuzzTrainfile(fuzztrianfile);
		tools.Const.setFuzzTestfile(fuzztestfile);
		tools.Const.setModelfile(modelfile);
		tools.Const.setRFobbfile(RFobbfile);
	}
	private static void printSet() {
		for(int i=0;i<modelist.length-1;i++)
			System.out.print(modelist[i]+",");
		System.out.println(modelist[modelist.length-1]);
		for(int i=0;i<setlist.length;i++)
		{
			double[] temp=set.getconfig(setlist[i]);
			if(temp!=null)
			{
				System.out.print(setlist[i]+":");
				for(int j=0;j<temp.length-1;j++)
					System.out.print(temp[j]+",");
				System.out.println(temp[temp.length-1]);
			}
				
		}
	}
}
