package testSample;

import java.util.ArrayList;

public class configerSet {
	//决策树参数
	private double[] alphalist;
	private double[] beltlist;
	private double[] setalist;
	private boolean[] fuzzSwitchlist= {true,false};
	//随机森林参数
	private double[] RFtreeNumlist;
	private double[] RFfeatureNumlist;
	private double[] RFselectTreeRatelist;
	private boolean[] RFdata= {true,false};
	public configerSet(ArrayList<readConfigData> data){
		for(int i=0;i<data.size();i++)
			setConfig(data.get(i));
	}
	private void setConfig(readConfigData data) {
		switch (data.getname()) {
		case "alpha":
			alphalist=data.getconfiglist();
			break;
		case "belt":
			beltlist=data.getconfiglist();
			break;
		case "seta":
			setalist=data.getconfiglist();
			break;
		case "RFtreeNum":
			RFtreeNumlist=data.getconfiglist();
			break;
		case "RFfeatureNum":
			RFfeatureNumlist=data.getconfiglist();
			break;
		case "RFselectTreeRate":
			RFselectTreeRatelist=data.getconfiglist();
			break;
		default:
			break;
		}			
	}
	public double[] getconfig(String set) {
		switch (set) {
		case "alpha":
			return getalphalist();
		case "belt":
			return getbeltlist();
		case "seta":
			return getsetalist();
		case "RFtreeNum":
			return getRFtreeNumlist();
		case "RFfeatureNum":
			return getRFfeatureNumlist();
		case "RFselectTreeRate":
			return getRFselectTreeRatelist();
		}
		return null;
	}
	private double[] getalphalist() {
		return alphalist;
	}
	private double[] getbeltlist() {
		return beltlist;
	}
	private double[] getsetalist() {
		return setalist;
	}
	public boolean[] getfuzzSwitchlist() {
		return fuzzSwitchlist;
	}
	private double[] getRFtreeNumlist() {
		return RFtreeNumlist;
	}
	private double[] getRFfeatureNumlist() {
		return RFfeatureNumlist;
	}
	private double[] getRFselectTreeRatelist() {
		return RFselectTreeRatelist;
	}
	public boolean[] getRFdata() {
		return RFdata;
	}
}
