package ID3;

import java.io.IOException;

import tools.readData;
import treeTools.TreeNode;

public class test {
private static double acc;
public static void main(String args[]) throws IOException {
	testModel(tools.Const.getFuzzTestfile());
}
public static void testModel(String filename) throws IOException {
	TreeNode root=treeTools.Tools.loadModel();
	//treeTools.Tools.printTree(root, 0);
	readData file=new readData(filename);	
	//file.printfile();
	testtree.testModel(file, root);
	treeTools.Assessment.fuzzACCtop1(testtree.getListReal(),testtree.getListPre());
	acc=treeTools.Assessment.getACC();
	//System.out.println(filename+" test finished");
}
public static double getACC() {
	return acc;
}
}
