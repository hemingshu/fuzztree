package ID3;

import java.util.ArrayList;

import tools.readData;
import treeTools.TreeNode;

public class buildtree {
	private static boolean once_flag=false;
	public static TreeNode trainModel(readData file) {
		once_flag=false;
		double[][] data=file.getData();
		double[][] label=file.getLabel();
		String[] titles=file.getTitleSet();
		String[] subtitles=file.getSubTitleSet();
		ArrayList<ArrayList<Integer>> schem=file.getSchem();
		TreeNode root=buildtree.builder(data, titles, subtitles, schem, label);
		return root;
	}
	private static TreeNode builder(double[][] data,String[] titles,String[] subtitles,ArrayList<ArrayList<Integer>> schem,double[][] label) {
		TreeNode node=new TreeNode();
		//所有特征都使用完了的叶子节点
		if(data[0].length==0)
		{
			SetLeaf(node,label,titles[0],subtitles);
			return node;
		}
		//非零数据数量不足时结束分类
		if(IsEnoughData(label))
		{
			String title=titles[titles.length-1];
			int num_label=schem.get(schem.size()-1).size();
			String[] subtitle=new String[num_label];
			for(int i=0;i<num_label;i++)				
				subtitle[i]=subtitles[subtitles.length-num_label+i];
			SetLeaf(node,label,title,subtitle);
			return node;
		}
		//置信水平较高时得到的叶子节点	
		if(!once_flag)
			once_flag=true;
		else
			if(IsLeaf(node,label,titles[titles.length-1],subtitles,data[0].length))
				return node;
		
		
	    int num_attributes=schem.size()-1;
	    int hangshu=data.length;
	    double gain=0;
	    int index=0;
	    int max=0;
	    int max_index=0;
	    double[] propobilities=new double[0];
	    //找出增益最大的特征作为节点
	    for(int i=0;i<num_attributes;i++)
	    {
	    	int num_features=schem.get(i).size();	    	
	    	double[][] attributes=new double[hangshu][num_features];
	    	
	    	for(int j=0;j<num_features;j++)
	    	{
	    		for(int m=0;m<hangshu;m++)	    		
	    			attributes[m][j]=data[m][index];
	    		index++;
	    	}
	    	
	    	double gain_temp=treeTools.entropy.getGain(attributes, label);
	    	//System.out.println(gain_temp);
	    	if(gain_temp>=gain)
	    	{
	    		propobilities=new double[num_features];
	    		propobilities=treeTools.entropy.getprobobility(attributes);
	    		gain=gain_temp;
	    		max=i;
	    		max_index=index-num_features;
	    	}
	    }
	    //将该节点作为树节点并调用递归程序
		int max_num_features=schem.get(max).size();
		node.setName(titles[max]);
		node.setIsleaf(false);

            	
		double[][] newdata=new double[hangshu][data[0].length-max_num_features];
		String[] newtitles=new String[titles.length-1];
		String[] newsubtitles=new String[subtitles.length-max_num_features];
		
		int count1=0;
		int count2=0;
		int count3=0;
		for(int j=0;j<num_attributes;j++)
		{
			if(j!=max)
			{
				int num_features=schem.get(j).size();
				newtitles[count1]=titles[j];
				count1++;
				
				for(int m=0;m<num_features;m++)
				{
					newsubtitles[count2]=subtitles[count3];					
					for(int n=0;n<hangshu;n++)
						newdata[n][count2]=data[n][count3];
					count2++;
					count3++;
				}			
			}
			else
			{
				count3+=max_num_features;	
			}
		}	
		newtitles[count1]=titles[num_attributes];
		int num_label=schem.get(num_attributes).size();
		for(int m=0;m<num_label;m++)
		{
			newsubtitles[count2]=subtitles[count3];
			count2++;
			count3++;
		}	
		
		ArrayList<ArrayList<Integer>> newschem=new ArrayList<ArrayList<Integer>>();
		for(int i=0;i<schem.size();i++)
		{
			ArrayList<Integer> temp=new ArrayList<Integer>();
			for(int j=0;j<schem.get(i).size();j++)
				temp.add(schem.get(i).get(j));
			newschem.add(temp);
		}
		newschem.remove(max);
		for(int i=0;i<max_num_features;i++)
		{
			node.addFeatures(subtitles[i+max_index]);
			node.addFeaProbobilities(propobilities[i]);
			
			double[] feature=new double[hangshu];
			for(int j=0;j<hangshu;j++)
				feature[j]=data[j][i+max_index];
			//获得最大属性作用后的标签集合
			double[][] newlabel=treeTools.entropy.getfuzzCollection(feature, label);
			//获得最大标签作用后的属性集合
			double[][] subdata=getfuzzSubData(feature,newdata,newschem);
			//当分出的数据集全部为0时，结束该分支的分类，生成叶子节点
			if(tools.mathtools.countSum2D(newlabel)==0)
			{
				node.setIsleaf(true);
				node.setName(titles[titles.length-1]);
				break;
			}
			node.addAttributes(builder(subdata,newtitles,newsubtitles,newschem,newlabel));			
		}
		return node;
	}
	private static double[][] getfuzzSubData(double[] feature,double[][] data,ArrayList<ArrayList<Integer>> schem) {
		int count=0;
		double[][] res=new double[data.length][data[0].length];
		for(int i=0;i<schem.size()-1;i++)
		{
			double[][] attributes=new double[feature.length][schem.get(i).size()];
			for(int m=0;m<attributes.length;m++)
				for(int n=0;n<attributes[0].length;n++)
					attributes[m][n]=data[m][count+n];
			if(!tools.Const.getfuzzSwitch())
				attributes=treeTools.entropy.getfuzzCollection(feature, attributes);
			for(int m=0;m<attributes.length;m++)
				for(int n=0;n<attributes[0].length;n++)
					res[m][count+n]=attributes[m][n];
			count+=schem.get(i).size();
		}
		return res;
	}
	private static void SetLeaf(TreeNode node,double[][] label,String name,String[] subtitles) {
		double[] probobilities=treeTools.entropy.getprobobility(label);
		int lieshu=probobilities.length;
		for(int i=0;i<lieshu;i++)
		{
			node.addProbobilities(probobilities[i]);
			node.addlabel(subtitles[i]);			
		}
		node.setMaxlabel();
		node.setMaxprobobility();
		node.setName(name);
		node.setIsleaf(true);
	}
	private static boolean IsLeaf(TreeNode node,double[][] label,String name,String[] subtitles,int num) {
		double[] probobilities=treeTools.entropy.getprobobility(label);
		int lieshu=probobilities.length;
		for(int i=0;i<lieshu;i++)
		{
			node.addProbobilities(probobilities[i]);
			node.addlabel(subtitles[num+i]);			
		}
		node.setMaxlabel();
		node.setMaxprobobility();
		if(node.getMaxprobobility()>=tools.Const.getBelt())
		{
			node.setName(name);
			node.setIsleaf(true);
			return true;
		}
		node.setIsleaf(false);
		//node.initFeatures();
		//node.initPropobilities();
		return false;
	}
	private static boolean IsEnoughData(double[][] label) {
		boolean res=false;
		int count=0;
		int hangshu=label.length;
		int lieshu=label[0].length;
		for(int i=0;i<hangshu;i++)
		{
			boolean temp=false;
			for(int j=0;j<lieshu;j++)
				temp=temp || (label[i][j]>0.0 );
			if(temp)
				count++;
		}
		if(count<tools.Const.getSeta())
			res=true;
		return res;
	}
}
