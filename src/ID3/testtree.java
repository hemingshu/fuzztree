package ID3;

import java.util.ArrayList;
import tools.readData;
import treeTools.TreeNode;

public class testtree {
private static double[] resultofData;
private static int num_feature;
private static double[][] listPre;
private static double[][] listReal;
public static void testModel(readData file,TreeNode root) {
	double[][] data=file.getData();
	listReal=file.getLabel();
	String[] titles=file.getTitleSet();
	String[] subtitles=file.getSubTitleSet();
	ArrayList<ArrayList<Integer>> schem=file.getSchem();
	//DecimalFormat df = new DecimalFormat( "0.0000");
	num_feature=listReal[0].length;
	listPre=new double[data.length][num_feature];
	for(int i=0;i<data.length;i++)
	{
		resultofData=new double[num_feature];
		ResultTest(data[i],titles,subtitles,schem,root,1.0);
		for(int j=0;j<num_feature;j++)
		{
			//System.out.print(df.format(resultofData[j])+" ");
			listPre[i][j]=resultofData[j];
		}			
		//System.out.println();
	}
}
public static double[][] getListReal() {
	return listReal;
}
public static double[][] getListPre() {
	return listPre;
}
private static void ResultTest(double[] data,String[] titles,String[] subtitles,ArrayList<ArrayList<Integer>> schem,TreeNode node,double propotity) {
	if(node.getIsleaf())
	{
		double[] res=new double[num_feature];
		for(int i=0;i<num_feature;i++)
		{
			res[i]=node.getPropobilities().get(i)*propotity;
			resultofData[i]+=res[i];				
		}		
		return;
	}
	
	int index=0;
	for(int i=0;i<titles.length;i++)
	{
		if(node.getName().equals(titles[i]))
		{
			index=i;
			break;
		}
	}
	String[] newtitles=new String[titles.length-1];
	String[] newsubtitles=new String[subtitles.length-schem.get(index).size()];
	double[] newdata=new double[data.length-schem.get(index).size()];
	int count1=0;
	int count2=0;
	int count3=0;
	int dataindex=0;
	for(int i=0;i<titles.length-1;i++)
	{
		if(i!=index) {
			newtitles[count1]=titles[i];
		    for(int j=0;j<schem.get(i).size();j++)
		    {
		    	newsubtitles[count2]=subtitles[count3];
		    	newdata[count2]=data[count3];
		    	count2++;
		    	count3++;
		    }
		    count1++;
		}
		else
		{
			dataindex=count3;
			count3+=schem.get(i).size();			
		}					
	}
	newtitles[count1]=titles[titles.length-1];
	for(int i=0;i<schem.get(schem.size()-1).size();i++)
		newsubtitles[i+count2]=subtitles[i+count3];
	ArrayList<ArrayList<Integer>> newschem=new ArrayList<ArrayList<Integer>>();
	for(int i=0;i<schem.size();i++)
	{
		ArrayList<Integer> temp=new ArrayList<Integer>();
		for(int j=0;j<schem.get(i).size();j++)
			temp.add(schem.get(i).get(j));
		newschem.add(temp);
	}
	newschem.remove(index);
	for(int i=0;i<node.getFeatures().size();i++)	
		ResultTest(newdata,newtitles,newsubtitles,newschem,node.getAttributes().get(i),propotity*data[dataindex+i]);	
}
}
