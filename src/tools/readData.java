package tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class readData {
	private String filename;
	private int feature_num=0;
	private double[][] data=new double[0][0];
	private double[][] label=new double[0][0];
	private ArrayList<String> titles=new ArrayList<String>();
	private ArrayList<ArrayList<String>> subtitles=new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<Integer>> schem=new ArrayList<ArrayList<Integer>>();
	private int[] label_list=new int[0];
 	public readData(String s) throws IOException {
		this.filename=s;
		readfile();
	}
	private void readfile() throws IOException {
		ArrayList<ArrayList<Double>> num=new ArrayList<ArrayList<Double>>();
		File file = new File(filename);
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(file));
		String s = null;
		while ((s = br.readLine()) != null) {
			if(s.contains("@"))
			{
				String[] line=s.split(":");
				titles.add(deleteChar(line[0],'@'));
				String[] subline=line[1].split(",");
				ArrayList<String> subtemp=new ArrayList<String>();
				for(int i=0;i<subline.length;i++)
					subtemp.add(subline[i]);
				subtitles.add(subtemp);
			}
			else
			{
				String[] line=s.split(",");
				ArrayList<Double> temp=new ArrayList<Double>();
				for(int i=0;i<line.length;i++)
					temp.add(Double.valueOf(line[i]));
				num.add(temp);
			}
		}
		int hangshu=num.size();
		int hangshu2=subtitles.size();
		for(int i=0;i<hangshu2;i++)
		{
			ArrayList<Integer> temp=new ArrayList<Integer>();
			int lieshu=subtitles.get(i).size();
			for(int j=0;j<lieshu;j++)
			{
				temp.add(j);
				feature_num++;
			}
			schem.add(temp);
		}
		int label_num=subtitles.get(hangshu2-1).size();
		data=new double[hangshu][feature_num-label_num];
		label=new double[hangshu][label_num];
		label_list=new int[label_num];
		//get features
		for(int i=0;i<hangshu;i++)			
			for(int j=0;j<feature_num-label_num;j++)
				data[i][j]=num.get(i).get(j);
		//get label
		for(int i=0;i<hangshu;i++)
		{
			int maxIndex=0;
			double max=0.0;
			for(int j=feature_num-label_num;j<feature_num;j++)
			{
				label[i][j-feature_num+label_num]=num.get(i).get(j);
				if(num.get(i).get(j)>max)
				{
					max=num.get(i).get(j);
					maxIndex=j-feature_num+label_num;
				}				
			}
			label_list[maxIndex]++;	
		}			
	}
	public int[] getDataNum() {
		return label_list;
	}
	public ArrayList<String> getTitles() {
		return titles;
	}
	public ArrayList<ArrayList<String>> getSubTitles(){
		return subtitles;
	}
	public double[][] getData(){
		return data;
	}
	private String deleteChar(String s,char a) {
		char[] ch=s.toCharArray();
		String res="";
		for(int i=0;i<ch.length;i++)
			if(ch[i]!=a)
				res=res+ch[i];
		return res;
	}
	public String[] getTitleSet() {
		int num=titles.size();
		String[] res=new String[num];
		for(int i=0;i<num;i++)
			res[i]=titles.get(i);
		return res;
	}
	public String[] getSubTitleSet() {	
		int hangshu=subtitles.size();
		String[] res=new String[feature_num];
		int count=0;
		for(int i=0;i<hangshu;i++)
		{
			int lieshu=subtitles.get(i).size();
			for(int j=0;j<lieshu;j++)
			{
				res[count]=subtitles.get(i).get(j);
				count++;
			}
		}			
		return res;
	}
	public ArrayList<ArrayList<Integer>> getSchem(){
		return schem;
	}
	public double[][] getLabel(){
		return label;
	}
	public void printfile() {
		for(int i=0;i<titles.size();i++)
			System.out.print(titles.get(i)+" ");
		System.out.println();
		for(int i=0;i<subtitles.size();i++)
			for(int j=0;j<subtitles.get(i).size();j++)
				System.out.print(subtitles.get(i).get(j)+" ");
		System.out.println();
		for(int i=0;i<data.length;i++)
		{
			for(int j=0;j<data[0].length;j++)
				System.out.print(data[i][j]+" ");
			for(int j=0;j<label[0].length;j++)
				System.out.print(label[i][j]+" ");
			System.out.println();
		}
	}
}
