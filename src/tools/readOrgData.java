package tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class readOrgData {
	private String filename;
	private double[][] data=new double[0][0];
	private String[] titles=new String[0];
	private int[] labellist;
	private int[] label;
	public readOrgData(String s) throws IOException {
		this.filename=s;
		readfile();
	}
	private  void readfile() throws IOException {		
		File file = new File(filename);
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<ArrayList<Double>> num=new ArrayList<ArrayList<Double>>();
		String s = null;
		s = br.readLine();
		titles=s.split(",");
		while ((s = br.readLine()) != null) {
			String[] line=s.split(",");
			ArrayList<Double> temp=new ArrayList<Double>();
			for(int i=0;i<line.length;i++)
				temp.add(Double.valueOf(line[i]));
			num.add(temp);
		}
		int hangshu=num.size();
		int lieshu=num.get(0).size();
		data=new double[hangshu][lieshu];
		for(int i=0;i<hangshu;i++)			
			for(int j=0;j<lieshu;j++)
				data[i][j]=num.get(i).get(j);
		label=new int[hangshu];
		for(int i=0;i<hangshu;i++)
			label[i]=(int) data[i][lieshu-1];
		
	}
	public int[] getlabel() {
		return label;
	}
	public int[] getlabellist() {
		ArrayList<Integer> labelclass=treeTools.entropy.getFeatures(label);
		labellist=new int[labelclass.size()];
		for(int i=0;i<label.length;i++)
			labellist[label[i]-labelclass.get(0)]++;
		return labellist;
	}
	public String[] getTitles() {
		return titles;
	}
	public double[][] getData(){
		return data;
	}
	public void printFile() {
		for(int i=0;i<titles.length;i++)
			System.out.print(titles[i]+" ");
		System.out.println();
		for(int i=0;i<data.length;i++)
		{
			for(int j=0;j<data[0].length;j++)
				System.out.print(data[i][j]+" ");
			System.out.println();
		}	
	}
}
