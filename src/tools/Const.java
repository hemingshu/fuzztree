package tools;

public class Const {
	//置信水平，隶属度小于该值的都置为0
	private static double alpha=0;
	//置信水平，概率大于该值的生成叶子节点
	private static double belt=1.0;
	//门限值，非零数据小于该值的生成叶子节点
	private static int seta=0;
	//模糊树改进开关
	private static boolean fuzzSwitch=true;
	//k-means聚类中心数
	private static int kmeans_k=3;
	//k-means迭代数
	private static int kmeans_r=200;
	//train和test文件数据的比例
	private static int train_test_rate=18;
	//随机森林树的数量
	private static int RFtreeNum=10;
	//随机森林属性的个数（与原有属性的差值）
	private static int RFfeatureNum=1;
	//随机数据的开关
	private static boolean IsRandomData=true;
	//随机森林选择树的比例(0.0~1.0)
	private static double RFselectTreeNum=0.1;
	//文件路径设置
	private static String workspacePath="B:\\yww\\ywwbs\\fuzzTree\\data1127\\RF1\\";
	private static String setPath="B:\\yww\\ywwbs\\fuzzTree\\data1127\\";
	private static String fuzztrainfile="fuzztrain.data";
	private static String fuzztestfile="fuzztest.data";
	private static String exacttrainfile="exactrain.data";
	private static String exacttestfile="exactest.data";
	private static String modelfile="tree.model";
	private static String Orgfile="sample.csv";
	private static String kmeansfile="sample.kmeans";
	private static String RFtrainfile="fuzztrain.data";
	private static String RFtestfile="fuzztest.data";
	private static String RFobbfile="obb.data";
	private static String testSamplefile="testSample.config";
	private static String resultfile="result.log";
	
	public static void setBelt(double d) {
		belt=d;
	}
	public static double getBelt() {
		return belt;
	}
	public static void setAlpha(double d) {
		alpha=d;
	}
	public static double getAlpha() {
		return alpha;
	}
	public static void setSeta(int num) {
		seta=num;
	}
	public static int getSeta() {
		return seta;
	}
	public static void setfuzzSwitch(boolean num) {
		fuzzSwitch=num;
	}
	public static boolean getfuzzSwitch() {
		return fuzzSwitch;
	}
	public static void setkmeans_k(int num) {
		kmeans_k=num;
	}
	public static int getkmeans_k() {
		return kmeans_k;
	}
	public static void setkmeans_r(int num) {
		kmeans_r=num;
	}
	public static int getkmeans_r() {
		return kmeans_r;
	}
	public static void setTTR(int num) {
		train_test_rate=num;
	}
	public static int getTTR() {
		return train_test_rate;
	}
	public static void setRFtreeNum(int num) {
		RFtreeNum=num;
	}
	public static int getRFtreeNum() {
		return RFtreeNum;
	}
	public static void setRFfeatureNum(int num) {
		RFfeatureNum=num;
	}
	public static int getRFfeatureNum() {
		return RFfeatureNum;
	}
	public static void setIsRandomData(boolean num) {
		IsRandomData=num;
	}
	public static boolean getIsRandomData() {
		return IsRandomData;
	}
	public static void setRFselectTreeNum(double num) {
		RFselectTreeNum=num;
	}
	public static double getRFselectTreeNum() {
		return RFselectTreeNum;
	}
	public static void setWorkspacePath(String s) {
		workspacePath=s;
	}
	public static void setFuzzTrainfile(String s) {
		fuzztrainfile=s;
	}
	public static String getFuzzTrainfile() {
		return workspacePath+fuzztrainfile;
	}
	public static void setFuzzTestfile(String s) {
		fuzztestfile=s;
	}
	public static String getFuzzTestfile() {
		return workspacePath+fuzztestfile;
	}
	public static void setExactTrainfile(String s) {
		exacttrainfile=s;
	}
	public static String getExactTrainfile() {
		return workspacePath+exacttrainfile;
	}
	public static void setExactTestfile(String s) {
		exacttestfile=s;
	}
	public static String getExactTestfile() {
		return workspacePath+exacttestfile;
	}
	public static void setRFtTrainfile(String s) {
		RFtrainfile=s;
	}
	public static String getRFTrainfile() {
		return workspacePath+RFtrainfile;
	}
	public static void setRFTestfile(String s) {
		RFtestfile=s;
	}
	public static String getRFTestfile() {
		return workspacePath+RFtestfile;
	}
	public static void setRFobbfile(String s) {
		RFobbfile=s;
	}
	public static String getRFobbfile() {
		return workspacePath+RFobbfile;
	}
	public static void setModelfile(String s) {
		modelfile=s;
	}
	public static String getModelfile() {
		return workspacePath+modelfile;
	}
	public static void setOrgfile(String s) {
		Orgfile=s;
	}
	public static String getOrgfile() {
		return setPath+Orgfile;
	}
	public static void setKmeansfile(String s) {
		kmeansfile=s;
	}
	public static String getKmeansfile() {
		return setPath+kmeansfile;
	}
	public static void settestSample(String s) {
		testSamplefile=s;
	}
	public static String gettestSample() {
		return setPath+testSamplefile;
	}
	public static void setresult(String s) {
		resultfile=s;
	}
	public static String getresult() {
		return setPath+resultfile;
	}
}
