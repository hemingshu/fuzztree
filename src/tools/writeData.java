package tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class writeData {
	public static void writeFuzzData(String[] titles,ArrayList<ArrayList<Integer>> schem,double[][] res,String filename) throws IOException {
		DecimalFormat df = new DecimalFormat( "0.0000");
		File file = new File(filename);
		@SuppressWarnings("resource")
		FileWriter fw = new FileWriter(file);
		//write title
		for(int i=0;i<titles.length;i++)
		{
			fw.write("@"+titles[i]+":");		
				if(schem.get(i).size()==2)
					fw.write("low,"+"high");
				else if(schem.get(i).size()==3)
					fw.write("low,"+"middle,"+"high");
				else
					for(int j=0;j<schem.get(i).size();j++)
						if(j!=schem.get(i).size()-1)
							fw.write((j+1)+",");
						else
							fw.write(String.valueOf(j+1));
			
				fw.flush();
				fw.write("\r\n");
		}
		//write data
		for(int i=0;i<res.length;i++) {
			for(int j=0;j<res[0].length;j++)
				if(j!=res[0].length-1)
					fw.write(df.format(res[i][j])+",");
				else
					fw.write(df.format(res[i][j]));
			fw.flush();
			fw.write("\r\n");
		}			
	}
	public static void writeExactData(String[] titles,int[][] res,String filename) throws IOException {
		File file = new File(filename);
		@SuppressWarnings("resource")
		FileWriter fw = new FileWriter(file);
		//write title
		for(int i=0;i<titles.length;i++)
			if(i!=titles.length-1)
				fw.write(titles[i]+",");
			else
				fw.write(titles[i]);
		fw.flush();
		fw.write("\r\n");
		//write data
		for(int i=0;i<res.length;i++) {
			for(int j=0;j<res[0].length;j++)
				if(j!=res[0].length-1)
					fw.write(res[i][j]+",");
				else
					fw.write(String.valueOf(res[i][j]));
			fw.flush();
			fw.write("\r\n");
		}			
	}
}
