package tools;

import java.util.ArrayList;

public class mathtools {
	public static int getMost(int[] data) {
		ArrayList<Integer> dataclass=treeTools.entropy.getFeatures(data);
		int[] count=new int[dataclass.size()];
		for(int i=0;i<data.length;i++)
			for(int j=0;j<count.length;j++)
				if(data[i]==dataclass.get(j))
				{
					count[j]++;
					break;
				}
		int max=0;
		int maxIndex=0;
		for(int j=0;j<count.length;j++)
			if(count[j]>max)
			{
				max=count[j];
				maxIndex=j;
			}
		return dataclass.get(maxIndex);
	}
	public static int getMaxIndex(double[] data,int num) {
		int[] index=new int[data.length];
		for(int i=0;i<data.length;i++)
			index[i]=i;
		QuickSort(index,data,0,data.length-1);
		int temp=num;
		if(num>1)
			while(data[index[data.length-temp]]==data[index[data.length-1]])
				temp++;
		return index[data.length-temp];
	}
	public static double getMaxNum(double[] data,double rate) {
		double[] newdata=deleSameNum(data);
		//所有数据都相同时，返回0.0，保证减完门限值后数据不为0
		if(newdata.length==1)
			return 0.0; 
		//设置和数据种类相同的index
		int[] index=new int[newdata.length];
		for(int i=0;i<newdata.length;i++)
			index[i]=i;
		//快速排序获得排序后的下标
		QuickSort(index,newdata,0,newdata.length-1);
		int num=(int) (newdata.length*rate);
		//如果取到前百分之0的数据，返回次大的数据，保证最大数据减完不为0
		if(num<2)
			num+=2;
		return newdata[index[newdata.length-num]];
	}
	private static double[] deleSameNum(double[] data) {
		ArrayList<Double> newdata=new ArrayList<Double>();
		for(int i=0;i<data.length;i++)
		{
			boolean flag=true;
			for(int j=0;j<newdata.size();j++)
				if(newdata.get(j)==data[i])
				{
					flag=false;
					break;
				}
			if(flag)
				newdata.add(data[i]);
		}
		double[] num=new double[newdata.size()];
		for(int i=0;i<newdata.size();i++)
			num[i]=newdata.get(i);
		return num;			
	}
	public static double getmin(double[] data) {
		double res=10000000000000.0;
		for(int i=0;i<data.length;i++)
			if(data[i]<res)
				res=data[i];
		return res;
	}
	public static double countSum2D(double[][] data) {
		double res=0;
		for(int i=0;i<data.length;i++)
			for(int j=0;j<data[0].length;j++)
				res+=data[i][j];
		return res;
	}
	public static double countSum(double[] data) {
		double res=0;
		for(int i=0;i<data.length;i++)
			res+=data[i];
		return res;
	}
	public static double log2(double num) {
		return Math.log(num)/Math.log(2);
	}
	public static void QuickSort(int[] index,double[] data,int left,int right) {
		if(left<right)
		{
			int pos=partition(index,data,left,right);
			QuickSort(index,data, left, pos - 1);
			QuickSort(index,data, pos + 1, right);
		}
	}
	private static int partition(int[] index,double[] data,int left,int right) {
		double temp=data[index[left]];
		while(left<right)
		{
			while(left<right && data[index[right]] >= temp)
				right--;
			swap(index,left,right);
			while (left<right && data[index[left]] <= temp)
				left++;
			swap(index,left,right);
		}
		return left;
	}
	private static void swap(int[] a,int i, int j)
	{
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
