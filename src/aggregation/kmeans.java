package aggregation;


import java.util.ArrayList;
import java.util.Arrays;


public class kmeans {
public static double[] Kmeans(double[] data,int k,int r){
	int numclass=countNumClass(data);
	if(numclass<k)
	{
		System.out.println("please set a k less than classes");
		return null;
	}
	double[] res =new double[k];
	RandomInit(data,res);
//	for(int i=0;i<res.length;i++)
//		System.out.print(res[i]+" ");
//	System.out.println();
	for(int i=0;i<r;i++)
	{
	    double[] sum =new double[k];
	    int[] count=new int[k];
		for(int m=0;m<data.length;m++)
		{
			int index=MaxIndex(data[m],res);
			sum[index]+=data[m];
			count[index]++;
		}
		boolean flag=true;
		for(int m=0;m<k;m++)
			if(count[m]!=0)
				if(Math.abs(res[m]-sum[m]/count[m])>0.00000000000001) {
					flag=false;
					res[m]=sum[m]/count[m];
				}
		if(flag)
			break;
	}
	Arrays.sort(res);
	return res;
}
private static int MaxIndex(double num,double[] res) {
	int index=0;
	double min=1000000000000000.0;
	for(int i=0;i<res.length;i++)
		if(Math.abs(res[i]-num)<min)
		{
			min=Math.abs(res[i]-num);
			index=i;
		}			
	return index;
}
private static int countNumClass(double[] data) {
	int res=0;
	ArrayList<Double> num=new ArrayList<Double>();
	for(int i=0;i<data.length;i++)
	{
		boolean flag=true;
		for(int j=0;j<num.size();j++)
			if(num.get(j)==data[i])
				flag=false;
		if(flag) {
			num.add(data[i]);
			res++;
		}
	}
	return res;
}
private static void RandomInit(double[] data,double[] res) {
	int hangshu=data.length;
	for(int i=0;i<res.length;i++)
	{		
		int index=(int) (hangshu*Math.random());
		boolean flag=true;
		for(int j=index;j<hangshu;j++)
		{
			for(int m=0;m<i;m++)
				if(data[j]==res[m])
					flag=false;
			if(flag)
			{
				res[i]=data[j];
				flag=false;
				break;
			}
			else
				flag=true;
		}
		if(flag)
		{
			for(int j=index-1;j>=0;j--)
			{
				for(int m=0;m<i;m++)
					if(data[j]==res[m])
						flag=false;
				if(flag)
				{
					res[i]=data[j];
					flag=false;
					break;
				}
				else
					flag=true;
			}
		}
	}
}
}
