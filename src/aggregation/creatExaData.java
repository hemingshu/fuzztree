package aggregation;

import java.io.IOException;
import java.util.ArrayList;

import tools.readOrgData;

public class creatExaData {
	public static void main(String args[]) throws IOException {
		AGGalgorthms.aggAlg("k-means");
		ExaData();
	}
	public static void ExaData() throws IOException {
		readOrgData file= new  readOrgData(tools.Const.getOrgfile());
		double[][] data=file.getData();
		ArrayList<ArrayList<Double>> aggNum=AGGalgorthms.getAggregationNum();
		int hangshu=data.length;
		int lieshu=data[0].length;
		int[][] res=new int[hangshu][lieshu];
		for(int i=0;i<hangshu;i++)
			for(int j=0;j<lieshu;j++)
				res[i][j]=getFeature(data[i][j],aggNum.get(j));
		String[] title=file.getTitles();
		for(int i=0;i<res.length;i++)
		{
			for(int j=0;j<res[0].length;j++)
				System.out.print(res[i][j]+" ");
			System.out.println();
		}
		writeExaData(title,res);
	}
	private static void writeExaData(String[] titles,int[][] res) throws IOException {
		int test_size=res.length/(tools.Const.getTTR()+1);
		int train_size=res.length-test_size;
		int[][] trainData=new int[train_size][res[0].length];
		int[][] testData=new int[test_size][res[0].length];
		int count=0;
		int count_test=0;
		int count_train=0;
		for(int i=0;i<res.length;i++)
		{
			if(count==tools.Const.getTTR())
			{
				testData[count_test]=res[i];
				count_test++;
				count=0;
			}
			else
			{
				trainData[count_train]=res[i];
				count_train++;
				count++;
			}
		}
		tools.writeData.writeExactData(titles,trainData,tools.Const.getExactTrainfile());
		tools.writeData.writeExactData(titles,testData,tools.Const.getExactTestfile());
	}
	private static int getFeature(double data,ArrayList<Double> schem) {
		int res=0;
		for(int i=0;i<schem.size();i++)
			if(data>schem.get(i))
				res++;
			else
				break;
		return res;
	}
}
