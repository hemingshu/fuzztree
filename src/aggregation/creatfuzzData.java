package aggregation;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import tools.readOrgData;

public class creatfuzzData {
public static void main(String args[]) throws IOException {
	AGGalgorthms.aggAlg("k-means");
	fuzzData();
}
public static void fuzzData() throws IOException {
	readOrgData file= new  readOrgData(tools.Const.getOrgfile());
	double[][] data=file.getData();
	ArrayList<ArrayList<Double>> aggNum=AGGalgorthms.getAggregationNum();
	ArrayList<ArrayList<Integer>> schem=new ArrayList<ArrayList<Integer>>();
	int hangshu=data.length;
	int lieshu=0;	
	for(int i=0;i<aggNum.size();i++) 
		lieshu+=aggNum.get(i).size();
	double[][] res=new double[hangshu][lieshu];
	
	int count=0;
	for(int i=0;i<aggNum.size();i++) {
		ArrayList<Integer> temp_schem=new ArrayList<Integer>();
		for(int j=0;j<data.length;j++)
		{			
			double[] features=TriFun(data[j][i],aggNum.get(i));
			for(int m=0;m<aggNum.get(i).size();m++)			
				res[j][count+m]=features[m];						
		}
		for(int m=0;m<aggNum.get(i).size();m++)
			temp_schem.add(m);
		schem.add(temp_schem);
		count+=aggNum.get(i).size();
	}
		
	DecimalFormat df = new DecimalFormat( "0.0000");
	for(int i=0;i<res.length;i++)
	{
		for(int j=0;j<res[0].length;j++)
			System.out.print(df.format(res[i][j])+" ");
		System.out.println();
	}
	String[] title=file.getTitles();
	writeFuzzData(title,schem,res);
}
private static void writeFuzzData(String[] titles,ArrayList<ArrayList<Integer>> schem,double[][] res) throws IOException {
	int test_size=res.length/(tools.Const.getTTR()+1);
	int train_size=res.length-test_size;
	double[][] trainData=new double[train_size][res[0].length];
	double[][] testData=new double[test_size][res[0].length];
	int count=0;
	int count_test=0;
	int count_train=0;
	for(int i=0;i<res.length;i++)
	{
		if(count==tools.Const.getTTR())
		{
			testData[count_test]=res[i];
			count_test++;
			count=0;
		}
		else
		{
			trainData[count_train]=res[i];
			count_train++;
			count++;
		}
	}
	tools.writeData.writeFuzzData(titles,schem,trainData,tools.Const.getFuzzTrainfile());
	tools.writeData.writeFuzzData(titles,schem,testData,tools.Const.getFuzzTestfile());
}
private static double[] TriFun(double data,ArrayList<Double> NUM) {
	double[] res=new double[NUM.size()];
	for(int i=0;i<NUM.size();i++)
	{
		if(i==0)
		{
			if(data<NUM.get(i))
				res[i]=1;
			else if(data>NUM.get(i+1))
				res[i]=0;
			else
				res[i]=(NUM.get(i+1)-data)/(NUM.get(i+1)-NUM.get(i));
		}
		else if(i==NUM.size()-1)
		{
			if(data>NUM.get(i))
				res[i]=1;
			else if(data<NUM.get(i-1))
				res[i]=0;
			else
				res[i]=(data-NUM.get(i-1))/(NUM.get(i)-NUM.get(i-1));
		}
		else
		{
			if(data>=NUM.get(i) && data<NUM.get(i+1))
				res[i]=(NUM.get(i+1)-data)/(NUM.get(i+1)-NUM.get(i));
			else if(data<NUM.get(i) && data>NUM.get(i-1))
				res[i]=(data-NUM.get(i-1))/(NUM.get(i)-NUM.get(i-1));
			else
				res[i]=0;
		}
	}
	return res;
}
}
