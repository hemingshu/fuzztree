package aggregation;

import java.io.IOException;
import java.util.ArrayList; 

import tools.readOrgData;

public class AGGalgorthms {
	private static ArrayList<ArrayList<Double>> aggregation_num;
	public static void main(String args[]) throws IOException {
		aggAlg("k-means");
	}
	public static void aggAlg(String alg) throws IOException {
		aggregation_num=new ArrayList<ArrayList<Double>>();
		readOrgData file= new  readOrgData(tools.Const.getOrgfile());	
		//file.printFile();
		double[][] data=file.getData();
		int lieshu=data[0].length;
		
		
		if(alg.equals("k-means")) {
			readOrgData kmeansfile=new readOrgData(tools.Const.getKmeansfile());
			double[][] set=kmeansfile.getData();
		    //kmeansfile.printFile();
			for(int i=0;i<lieshu;i++)
			{
				tools.Const.setkmeans_k((int) set[0][i]);
				tools.Const.setkmeans_r((int) set[1][i]);
				double[] data0=new double[data.length];
				for(int j=0;j<data.length;j++)
					data0[j]=data[j][i];
				double[] res=kmeans.Kmeans(data0, tools.Const.getkmeans_k(),tools.Const.getkmeans_r());
				ArrayList<Double> temp=new ArrayList<Double>();
				for(int j=0;j<res.length;j++)
				{
					System.out.print(res[j]+" ");
					temp.add(res[j]);
				}
				System.out.println();
				aggregation_num.add(temp);
			}
		}
	}
	public static ArrayList<ArrayList<Double>> getAggregationNum(){
		return aggregation_num;
	}
	
}
