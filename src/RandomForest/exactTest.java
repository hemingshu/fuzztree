package RandomForest;

import java.io.IOException;

import tools.readOrgData;
import treeTools.Assessment;

public class exactTest {
	private static String modelname=tools.Const.getModelfile();
	private static String testfilename=tools.Const.getExactTestfile();
	private static String trainfilename=tools.Const.getExactTrainfile();
	private static String obbfilename=tools.Const.getRFobbfile();
	private static int[] listPre;
	private static int[][] listPreEach;
	private static double[][][] ProlistEach;
	private static double[][] Prolist;
	private static int[] listReal;
	private static double AveACC;
	private static double mostACC;
	private static double weightACC;
	
	public static void main(String args[]) throws IOException {
		test();
	}
	public static void test() throws IOException{
		readOrgData file=new readOrgData(tools.Const.getExactTestfile());
		setlistReal(file.getlabel(),file.getlabellist());
		double[] treewight=new double[tools.Const.getRFtreeNum()];
		for(int i=0;i<tools.Const.getRFtreeNum();i++)
		{
			//System.out.println("--------------------tree"+(i+1)+"--------------------");
			tools.Const.setExactTrainfile(RFTools.creatfilename(i+1, trainfilename));
			readOrgData trainfile=new readOrgData(tools.Const.getExactTrainfile());
			String testfile_name=RFTools.creatfilename(i+1, testfilename);
			tools.Const.setExactTestfile(testfile_name);
			tools.Const.setModelfile(RFTools.creatfilename(i+1, modelname));
			tools.Const.setRFobbfile(RFTools.creatfilename(i+1, obbfilename));
			setTestfile(trainfile,file);
			OrgID3.test.testModel(tools.Const.getExactTestfile());			
			addPre(i);
			if(tools.Const.getIsRandomData())
			{
				OrgID3.test.testModel(tools.Const.getRFobbfile());
				treewight[i]=Assessment.getACC();
			}
		}
		//System.out.println("--------------------RFtree SUM--------------------");		
		//System.out.print("most result:");
		mostRes();
		treeTools.Assessment.exactACCtop1(listReal, listPre);
		mostACC=treeTools.Assessment.getACC();
		//System.out.print("average result:");
		AverageRes();		
		treeTools.Assessment.exactACCtop1(listReal, listPre);
		AveACC=treeTools.Assessment.getACC();
		if(tools.Const.getIsRandomData())
		{
			//System.out.print("Obb-ACC weight result:");
			setACCOBBWeightPrelist(treewight);
			treeTools.Assessment.exactACCtop1(listReal, listPre);
			weightACC=treeTools.Assessment.getACC();
		}			
	}
	public static double getAveACC() {
		return AveACC;
	}
	public static double getMostACC() {
		return mostACC;
	}
	public static double getWightACC() {
		return weightACC;
	}
	private static void setACCOBBWeightPrelist(double[] weight) {
		double sum=0;
		double threshold=tools.mathtools.getMaxNum(weight, tools.Const.getRFselectTreeNum());
		for(int i=0;i<Prolist.length;i++)
			for(int j=0;j<Prolist[0].length;j++)
				Prolist[i][j]=0;
		for(int m=0;m<weight.length;m++)
		{
			weight[m]=weight[m]-threshold;
			if(weight[m]<0)
				weight[m]=0;
			sum+=weight[m];
		}
			
		for(int i=0;i<Prolist.length;i++)
			for(int j=0;j<Prolist[0].length;j++)
				for(int m=0;m<weight.length;m++)				
					Prolist[i][j]+=weight[m]*ProlistEach[i][j][m];				
		for(int i=0;i<Prolist.length;i++)
			for(int j=0;j<Prolist[0].length;j++)
				Prolist[i][j]=Prolist[i][j]/sum;		
		for(int i=0;i<Prolist.length;i++)
			listPre[i]=tools.mathtools.getMaxIndex(Prolist[i], 1);
//		for(int i=0;i<Prolist.length;i++)
//		{
//			for(int j=0;j<Prolist[0].length;j++)
//				System.out.print(Prolist[i][j]+" ");
//			System.out.println(listPre[i]);
//		}			
	}
	private static void mostRes() {
		int hangshu=listPreEach.length;
		for(int i=0;i<hangshu;i++)
			listPre[i]=tools.mathtools.getMost(listPreEach[i]);
	}
	private static void AverageRes() {
		int hangshu=Prolist.length;
		int lieshu=Prolist[0].length;
		for(int i=0;i<hangshu;i++)
			for(int j=0;j<lieshu;j++)
				Prolist[i][j]=0;
		for(int i=0;i<hangshu;i++)
			for(int j=0;j<lieshu;j++)
				for(int m=0;m<tools.Const.getRFtreeNum();m++)
					Prolist[i][j]+=ProlistEach[i][j][m];
		for(int i=0;i<hangshu;i++)
		{
			for(int j=0;j<lieshu;j++)			
				Prolist[i][j]=Prolist[i][j]/tools.Const.getRFtreeNum();
			listPre[i]=tools.mathtools.getMaxIndex(Prolist[i], 1);
		}		
				
	}
	private static void addPre(int num) {
		int hangshu=OrgID3.testtree.getListPre().length;
		for(int i=0;i<hangshu;i++)
		{
			listPreEach[i][num]=OrgID3.testtree.getListPre()[i];
			for(int j=0;j<OrgID3.testtree.getProlist()[0].length;j++)
				ProlistEach[i][j][num]=OrgID3.testtree.getProlist()[i][j];
		}			
	}
	private static void setTestfile(readOrgData trainfile,readOrgData testfile) throws IOException {
		String[] titles=trainfile.getTitles();
		int count=0;
		int[][] data=new int[testfile.getData().length][titles.length];
		for(int i=0;i<testfile.getTitles().length;i++)
			if(testfile.getTitles()[i].equals(titles[count]))
			{
				for(int j=0;j<testfile.getData().length;j++)
						data[j][count]=(int) testfile.getData()[j][i];
				count++;
			}
		tools.writeData.writeExactData(titles,data,tools.Const.getExactTestfile());
	}
	private static void setlistReal(int[] label,int[] labellist) {
		listReal=new int[label.length];
		listPre=new int[label.length];
		listPreEach=new int[label.length][tools.Const.getRFtreeNum()];
		Prolist=new double[label.length][labellist.length];
		ProlistEach=new double[label.length][labellist.length][tools.Const.getRFtreeNum()];
		for(int i=0;i<label.length;i++)
			listReal[i]=label[i];		
	}
	public static int[] getlistReal() {
		return listReal;
	}
}
