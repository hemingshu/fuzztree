package RandomForest;

import java.io.IOException;
import java.util.ArrayList;

import tools.readData;
import treeTools.Assessment;

public class fuzztest {
	private static String modelname=tools.Const.getModelfile();
	private static String testfilename=tools.Const.getFuzzTestfile();
	private static String trainfilename=tools.Const.getFuzzTrainfile();
	private static String obbfilename=tools.Const.getRFobbfile();
	private static double[][] listPre;
	private static double[][][] listPreEach;
	private static double[][] listReal;
	private static double AveACC;
	private static double weightACC;
	
	public static void main(String args[]) throws IOException {
		test();
	}
	public static void test() throws IOException{
		readData file=new readData(tools.Const.getFuzzTestfile());
		setlistReal(file.getLabel());
		double[] treewight=new double[tools.Const.getRFtreeNum()];
		for(int i=0;i<tools.Const.getRFtreeNum();i++)
		{
			//System.out.println("--------------------tree"+(i+1)+"--------------------");
			tools.Const.setFuzzTrainfile(RFTools.creatfilename(i+1, trainfilename));
			readData trainfile=new readData(tools.Const.getFuzzTrainfile());
			String testfile_name=RFTools.creatfilename(i+1, testfilename);
			tools.Const.setFuzzTestfile(testfile_name);
			tools.Const.setModelfile(RFTools.creatfilename(i+1, modelname));
			tools.Const.setRFobbfile(RFTools.creatfilename(i+1, obbfilename));
			setTestfile(trainfile,file);			
			ID3.test.testModel(tools.Const.getFuzzTestfile());
			addPrelist();			
			setPrelistEach(i);
			if(tools.Const.getIsRandomData())
			{
				ID3.test.testModel(tools.Const.getRFobbfile());
				treewight[i]=Assessment.getACC();
			}
		}
		//System.out.println("--------------------RFtree SUM--------------------");
		setPrelist();
		//System.out.print("average result:");
		treeTools.Assessment.fuzzACCtop1(listReal, listPre);
		AveACC=treeTools.Assessment.getACC();
		if(tools.Const.getIsRandomData())
		{
			setACCOBBWeightPrelist(treewight);
			//System.out.print("ACC-obb weighted result:");
			treeTools.Assessment.fuzzACCtop1(listReal, listPre);
			weightACC=treeTools.Assessment.getACC();
		}
		
	}
	public static double getAveACC() {
		return AveACC;
	}
	public static double getWightACC() {
		return weightACC;
	}
	private static void setACCOBBWeightPrelist(double[] weight) {
		double sum=0;
		double threshold=tools.mathtools.getMaxNum(weight, tools.Const.getRFselectTreeNum());
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				listPre[i][j]=0;
		for(int m=0;m<weight.length;m++)
		{
			weight[m]=weight[m]-threshold;
			if(weight[m]<0)
				weight[m]=0;
			sum+=weight[m];
		}
			
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				for(int m=0;m<weight.length;m++)				
					listPre[i][j]+=weight[m]*listPreEach[i][j][m];				
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				listPre[i][j]=listPre[i][j]/sum;
	}
	private static void setPrelistEach(int num) {
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				listPreEach[i][j][num]=ID3.testtree.getListPre()[i][j];
	}
	private static void addPrelist() {
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				listPre[i][j]+=ID3.testtree.getListPre()[i][j];
	}
	private static void setPrelist() {
		for(int i=0;i<listPre.length;i++)
			for(int j=0;j<listPre[0].length;j++)
				listPre[i][j]=listPre[i][j]/tools.Const.getRFtreeNum();
	}
	private static void setTestfile(readData trainfile,readData testfile) throws IOException {
		String[] titles=trainfile.getTitleSet();
		ArrayList<ArrayList<Integer>> schem=trainfile.getSchem();
		//count size of data
		int featureNum=0;
		for(int i=0;i<schem.size();i++)
			for(int j=0;j<schem.get(i).size();j++)
				featureNum++;
		int hangshu=testfile.getData().length;
		double[][] data=new double[hangshu][featureNum];
		//creat data
		int count=0;
		int count1=0;
		int count2=0;
		//copy features
		for(int i=0;i<testfile.getTitleSet().length-1;i++)
			if(testfile.getTitleSet()[i].equals(titles[count]))
			{
				count++;
				for(int j=0;j<testfile.getSchem().get(i).size();j++)
					for(int m=0;m<hangshu;m++)
						data[m][count2+j]=testfile.getData()[m][count1+j];
				count1+=testfile.getSchem().get(i).size();
				count2+=testfile.getSchem().get(i).size();
			}
			else
				count1+=testfile.getSchem().get(i).size();
		//copy labels
		for(int j=0;j<schem.get(schem.size()-1).size();j++)
			for(int m=0;m<hangshu;m++)
				data[m][count2+j]=testfile.getLabel()[m][j];
		//write to file
		tools.writeData.writeFuzzData(titles, schem, data, tools.Const.getFuzzTestfile());
	}
	private static void setlistReal(double[][] label) {
		listReal=new double[label.length][label[0].length];
		listPre=new double[label.length][label[0].length];
		listPreEach=new double[label.length][label[0].length][tools.Const.getRFtreeNum()];
		for(int i=0;i<label.length;i++)
			for(int j=0;j<label[0].length;j++)
				listReal[i][j]=label[i][j];
	}
	public static double[][] getlistReal(){
		return listReal;
	}
}
