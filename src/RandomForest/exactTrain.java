package RandomForest;

import java.io.IOException;

import tools.readOrgData;

public class exactTrain {
	private static String modelname=tools.Const.getModelfile();
	public static void main(String args[]) throws IOException {
		train();
	}
	public static void train() throws IOException {
		readOrgData file=new readOrgData(tools.Const.getExactTrainfile());
		for(int i=0;i<tools.Const.getRFtreeNum();i++)
		{
			//System.out.println("--------------------tree"+(i+1)+"--------------------");
			int attributeNum=file.getTitles().length-1-tools.Const.getRFfeatureNum();
			RFTools RFdata=new RFTools(file.getlabellist(),attributeNum);
			creatExactfile.creatExact(file,RFdata,i+1);
			tools.Const.setModelfile(RFTools.creatfilename(i+1, modelname));
			OrgID3.train.trainModel();
			if(tools.Const.getIsRandomData())
				OrgID3.test.testModel(tools.Const.getRFobbfile());
		}
	}
}
