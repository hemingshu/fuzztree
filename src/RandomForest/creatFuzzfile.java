package RandomForest;

import java.io.IOException;
import java.util.ArrayList;
import tools.readData;

public class creatFuzzfile {
	private static readData file;
	private static RFTools RFdata;
	private static String[] titles;
	private static ArrayList<ArrayList<Integer>> schem;
	private static double[][] data;
	private static double[][] obb;
	private static String FuzzTrainfilename=tools.Const.getFuzzTrainfile();
	private static String obbfilename=tools.Const.getRFobbfile();
	public static void creatfuzz(readData fileSet,RFTools RFdataSet,int treeNum) throws IOException {
		file=fileSet;
		RFdata=RFdataSet;
		schem=new ArrayList<ArrayList<Integer>>();
		titles=new String[RFdata.getAttributes().length+1];
		//printFeatures();
		creatTitles();
	    creatData();
		tools.Const.setFuzzTrainfile(RFTools.creatfilename(treeNum,FuzzTrainfilename));
		tools.Const.setRFobbfile(RFTools.creatfilename(treeNum,obbfilename));
		tools.writeData.writeFuzzData(titles, schem, data,tools.Const.getFuzzTrainfile());
		if(tools.Const.getIsRandomData())
			tools.writeData.writeFuzzData(titles, schem, obb,tools.Const.getRFobbfile());
	}
	private static void creatData() {
		int feature_num=0;
		for(int i=0;i<schem.size();i++)
			feature_num+=schem.get(i).size();
		data=new double[file.getData().length][feature_num];
		if(tools.Const.getIsRandomData())
			obb=new double[RFdata.getObb().size()][feature_num];
		int count=0;
		int count1=0;
		int count2=0;
		//creat features
		for(int i=0;i<file.getSchem().size()-1;i++)
		{
			if(i==RFdata.getAttributes()[count])
			{
				count++;
				for(int j=0;j<file.getSchem().get(i).size();j++)
				{
					for(int m=0;m<data.length;m++)
						if(tools.Const.getIsRandomData())
							data[m][count2+j]=file.getData()[RFdata.getData()[m]][count1+j];
						else
							data[m][count2+j]=file.getData()[m][count1+j];
					if(tools.Const.getIsRandomData())
						for(int m=0;m<RFdata.getObb().size();m++)
							obb[m][count2+j]=file.getData()[RFdata.getObb().get(m)][count1+j];
				}					
				count1+=file.getSchem().get(i).size();
				count2+=file.getSchem().get(i).size();
			}
			else
				count1+=file.getSchem().get(i).size();
			if(count==RFdata.getAttributes().length)
				break;
		}
		//creat labels
		for(int j=0;j<file.getSchem().get(file.getSchem().size()-1).size();j++)
		{
			for(int m=0;m<data.length;m++)
				if(tools.Const.getIsRandomData())
					data[m][count2+j]=file.getLabel()[RFdata.getData()[m]][j];
				else
					data[m][count2+j]=file.getLabel()[m][j];
			if(tools.Const.getIsRandomData())
				for(int m=0;m<RFdata.getObb().size();m++)
					obb[m][count2+j]=file.getLabel()[RFdata.getObb().get(m)][j];
		}					
	}
	private static void creatTitles() {
		for(int i=0;i<RFdata.getAttributes().length;i++)
		{
			titles[i]=file.getTitleSet()[RFdata.getAttributes()[i]];
			schem.add(file.getSchem().get(RFdata.getAttributes()[i]));
		}
		titles[titles.length-1]=file.getTitleSet()[file.getTitleSet().length-1];
		schem.add(file.getSchem().get(file.getSchem().size()-1));
//		System.out.print("random features: ");
//		for(int i=0;i<titles.length;i++)
//			System.out.print(titles[i]+" ");
//		System.out.println();
	}
	@SuppressWarnings("unused")
	private static void printFeatures() {
		System.out.print("random features: ");
		for(int i=0;i<RFdata.getAttributes().length;i++)
			System.out.print(RFdata.getAttributes()[i]+" ");
		System.out.println();
	}

}
