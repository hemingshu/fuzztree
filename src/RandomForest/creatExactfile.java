package RandomForest;

import java.io.IOException;

import tools.readOrgData;

public class creatExactfile {
	private static readOrgData file;
	private static RFTools RFdata;
	private static String[] titles;
	private static int[][] data;
	private static int[][] obb;
	private static String FuzzTrainfilename=tools.Const.getExactTrainfile();
	private static String obbfilename=tools.Const.getRFobbfile();
	public static void creatExact(readOrgData fileSet,RFTools RFdataSet,int treeNum) throws IOException {
		file=fileSet;
		RFdata=RFdataSet;
		//printFeatures();
		creatTitles();
		creatData();
		tools.Const.setExactTrainfile(RFTools.creatfilename(treeNum,FuzzTrainfilename));
		tools.Const.setRFobbfile(RFTools.creatfilename(treeNum,obbfilename));
		tools.writeData.writeExactData(titles, data,tools.Const.getExactTrainfile());
		if(tools.Const.getIsRandomData())
			tools.writeData.writeExactData(titles, obb,tools.Const.getRFobbfile());
	}
	private static void creatData() {
		data=new int[file.getData().length][titles.length];
		if(tools.Const.getIsRandomData())
			obb=new int[RFdata.getObb().size()][titles.length];
		int count=0;
		for(int i=0;i<file.getTitles().length;i++)
			if(file.getTitles()[i].equals(titles[count]))
			{
				for(int j=0;j<file.getData().length;j++)
					if(tools.Const.getIsRandomData())
						data[j][count]=(int) file.getData()[RFdata.getData()[j]][i];
					else
						data[j][count]=(int) file.getData()[j][i];
				if(tools.Const.getIsRandomData())
					for(int j=0;j<RFdata.getObb().size();j++)
						obb[j][count]=(int) file.getData()[RFdata.getObb().get(j)][i];
				count++;
			}
	}		
	private static void creatTitles() {
		titles=new String[RFdata.getAttributes().length+1];
		for(int i=0;i<RFdata.getAttributes().length;i++)		
			titles[i]=file.getTitles()[RFdata.getAttributes()[i]];		
		titles[titles.length-1]=file.getTitles()[file.getTitles().length-1];
		
//		System.out.print("random features: ");
//		for(int i=0;i<titles.length;i++)
//			System.out.print(titles[i]+" ");
//		System.out.println();
	}
	@SuppressWarnings("unused")
	private static void printFeatures() {
		System.out.print("random features: ");
		for(int i=0;i<RFdata.getAttributes().length;i++)
			System.out.print(RFdata.getAttributes()[i]+" ");
		System.out.println();
	}
}
