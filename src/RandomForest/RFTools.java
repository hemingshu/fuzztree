package RandomForest;

import java.util.ArrayList;
import java.util.Arrays;

public class RFTools {
	private int[] num_data;
	private int num_attribute;
	private int[] dataIndex;
	private int[] attIndex;
	ArrayList<Integer> obbIndex=new ArrayList<Integer>();
	public RFTools(int[] dataNum,int attributeNum) {
		this.num_data=dataNum;
		this.num_attribute=attributeNum;
		randomData();
		randomSchem();
	}
	private void randomData() {
		int sum=0;
		for(int i=0;i<num_data.length;i++)
			sum+=num_data[i];
		dataIndex=new int[sum];
		boolean[] isUsed=new boolean[sum];
		for(int i=0;i<sum;i++)
			isUsed[i]=false;
		//get train file
		int count=0;		
		for(int i=0;i<num_data.length;i++) {
			for(int j=0;j<num_data[i];j++)
			{
				dataIndex[j+count]=count+(int)(num_data[i]*Math.random());
				isUsed[dataIndex[j+count]]=true;
			}
			count+=num_data[i];
		}
		//get obb file
		for(int i=0;i<sum;i++)
			if(!isUsed[i])
				obbIndex.add(i);		
	}
	private void randomSchem() {
		int featureNum=num_attribute+tools.Const.getRFfeatureNum();
		attIndex=new int[num_attribute];
		ArrayList<Integer> index=new ArrayList<Integer>();
		for(int i=0;i<featureNum;i++)
			index.add(i);
		for(int i=0;i<num_attribute;i++)
		{
//			for(int j=0;j<index.size();j++)
//				System.out.print(index.get(j)+" ");
//			System.out.println();
			int random_index=(int)((featureNum-i)*Math.random());
			attIndex[i]=index.get(random_index);
			index.remove(random_index);			
		}
		Arrays.sort(attIndex);
	}
	public int[] getData() {
		return dataIndex;
	}
	public int[] getAttributes() {
		return attIndex;
	}
	public ArrayList<Integer> getObb(){
		return obbIndex;
	}
	public static String creatfilename(int num, String filename) {
		String res="";
		//System.out.println(filename);
		String[] temp0=filename.split("\\\\");
		//System.out.println(temp0[temp0.length-1]);
		String[] temp=temp0[temp0.length-1].split("\\.");
		if(num>-1)
			res=temp[0]+num+"."+temp[1];
		else
			res=temp[0]+"."+temp[1];
		return res;		
	}
}
