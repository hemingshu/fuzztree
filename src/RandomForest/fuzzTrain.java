package RandomForest;

import java.io.IOException;

import tools.readData;

public class fuzzTrain {
	private static String modelname=tools.Const.getModelfile();
	public static void main(String args[]) throws IOException {
		train();
	}
	public static void train() throws IOException {
		readData file=new readData(tools.Const.getFuzzTrainfile());
		for(int i=0;i<tools.Const.getRFtreeNum();i++)
		{
			//System.out.println("--------------------tree"+(i+1)+"--------------------");
			int attributeNum=file.getTitleSet().length-1-tools.Const.getRFfeatureNum();
			RFTools RFdata=new RFTools(file.getDataNum(),attributeNum);
			creatFuzzfile.creatfuzz(file,RFdata,i+1);
			tools.Const.setModelfile(RFTools.creatfilename(i+1, modelname));
			ID3.train.trainModel();
			if(tools.Const.getIsRandomData())
				ID3.test.testModel(tools.Const.getRFobbfile());
		}
	}
	
}
