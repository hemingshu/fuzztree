package treeTools;

import java.util.ArrayList;

public class TreeNode {
	private  String name;
	private  boolean isleaf;
	private  ArrayList<String> features=new ArrayList<String>();
	private  ArrayList<Double> fea_probobilities=new ArrayList<Double>();
	private  ArrayList<TreeNode> attributes=new ArrayList<TreeNode>();
	private  ArrayList<String> label=new ArrayList<String>();
	private  ArrayList<Double> probobilities=new ArrayList<Double>();
	private  String maxfeature;
	private  double maxprobobility;
	
	public void setName(String s) {
		name=s;
	}
	public  String getName() {
		return name;
	}
	public  void setIsleaf(boolean b) {
		isleaf=b;
	}
	public  boolean getIsleaf() {
		return isleaf;
	}
	public  void addFeatures(String s) {
		features.add(s);
	}
	public  ArrayList<String> getFeatures(){
		return features;
	}
	public  void initFeatures() {
		features=new ArrayList<String>(); 
	}
	public  void addlabel(String s) {
		label.add(s);
	}
	public  ArrayList<String> getlabel(){
		return label;
	}
	public void setlabel(ArrayList<String> s) {
		label=s;
	}
	public  void initlabel() {
		label=new ArrayList<String>(); 
	}
	public  void addProbobilities(double d) {
		probobilities.add(d);
	}
	public  ArrayList<Double> getPropobilities(){
		return probobilities;
	}
	public void setpropobilities(ArrayList<Double> d) {
		probobilities=d;
	}
	public  void initPropobilities() {
		probobilities=new ArrayList<Double>(); 
	}
	public  void addAttributes(TreeNode t) {
		attributes.add(t);
	}
	public  ArrayList<TreeNode> getAttributes() {
		return attributes;
	}
	public  void addFeaProbobilities(double d) {
		fea_probobilities.add(d);
	}
	public  ArrayList<Double> getFeaPropobilities(){
		return fea_probobilities;
	}
	public  void initFeaPropobilities() {
		fea_probobilities=new ArrayList<Double>(); 
	}
	public  void setMaxlabel() {
		int index=0;
		double max=0;
		for(int i=0;i<probobilities.size();i++)
			if(probobilities.get(i)>max)
			{
				max=probobilities.get(i);
				index=i;
			}
		maxfeature=label.get(index);
	}
	public  String getMaxlabel() {
		return maxfeature;
	}
	public  void setMaxprobobility() {
		int index=0;
		double max=0;
		for(int i=0;i<probobilities.size();i++)
			if(probobilities.get(i)>max)
			{
				max=probobilities.get(i);
				index=i;
			}
		maxprobobility=probobilities.get(index);
	}
	public  double getMaxprobobility() {
		return maxprobobility;
	}
}
