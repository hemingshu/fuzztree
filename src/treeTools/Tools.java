package treeTools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Tools {
	private static int num_leaf;
	private static int num_node;
	private static int maxdepth;
	public static void initNumleaf() {
		num_leaf=0;
	}
	public static void initNumnodes() {
		num_node=0;
	}
	public static void initMaxdepth() {
		maxdepth=0;
	}
	public static int getNumleaf() {
		return num_leaf;
	}
	public static int getNumnode() {
		return num_node;
	}
	public static int getMaxdepth() {
		return maxdepth;
	}
 	public static void printTree(TreeNode node,int depth) {
		DecimalFormat df = new DecimalFormat( "0.0000");
		System.out.println("------ "+node.getName()+":");		
		ArrayList<String> labels=node.getlabel();
		ArrayList<Double> probobilities=node.getPropobilities();
		System.out.print("                                      "+node.getMaxlabel()+":"+df.format(node.getMaxprobobility()));
		System.out.print("(");
		for(int i=0;i<labels.size();i++)
			System.out.print(labels.get(i)+":"+df.format(probobilities.get(i))+",");
		System.out.println(")");
		
		if(node.getIsleaf())			
			return;
		
		ArrayList<String> features=node.getFeatures();
		ArrayList<Double> fea_probobilities=node.getFeaPropobilities();
		ArrayList<TreeNode> attributes=node.getAttributes();
		for(int i=0;i<features.size();i++)
		{
			System.out.print("------");
			for(int j=0;j<=depth;j++)
				System.out.print(" | ");
			System.out.print(features.get(i)+":"+df.format(fea_probobilities.get(i)));
			printTree( attributes.get(i),depth+1);
		}
	}
	public static void savemodel(TreeNode root) throws IOException {
		File file = new File(tools.Const.getModelfile());
		FileWriter fw = new FileWriter(file);
		writeModel(root,fw,0);
		fw.close();
	}
	private static void writeModel(TreeNode node,FileWriter fw,int depth) throws IOException {
		DecimalFormat df = new DecimalFormat( "0.0000");
		num_node++;
		testSample.sample.writeline(fw, "@"+node.getName()+":");
		ArrayList<String> labels=node.getlabel();
		ArrayList<Double> probobilities=node.getPropobilities();
		fw.write("#"+node.getMaxlabel()+":"+df.format(node.getMaxprobobility()));
		fw.flush();
		fw.write("(");
		fw.flush();
		for(int i=0;i<labels.size();i++)
		{
			fw.write(labels.get(i)+":"+df.format(probobilities.get(i)));
			if(i!=labels.size()-1)
				fw.write(",");
			fw.flush();
		}
		testSample.sample.writeline(fw, ")");
		
		if(node.getIsleaf())
		{
			num_leaf++;
			if(depth>=maxdepth)
				maxdepth=depth;
			return;
		}
			
		
		ArrayList<String> features=node.getFeatures();
		ArrayList<Double> fea_probobilities=node.getFeaPropobilities();
		ArrayList<TreeNode> attributes=node.getAttributes();
		for(int i=0;i<features.size();i++)
		{
			for(int j=0;j<=depth+1;j++)
			{
				fw.write("@");
				fw.flush();				
			}				
			fw.write(features.get(i)+":"+df.format(fea_probobilities.get(i))+",");
			fw.flush();
			writeModel( attributes.get(i),fw,depth+1);
		}
	}
	public static TreeNode loadModel() throws IOException {
		modelline[] lines=AnalysisModel();
	    TreeNode root=readModel(lines);
	    return root;
	}
	private static modelline[] AnalysisModel() throws IOException{
		File file = new File(tools.Const.getModelfile());
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new FileReader(file));
		String s = null;
		ArrayList<String> titles=new ArrayList<String>();
		ArrayList<String> feas=new ArrayList<String>();
		while((s=br.readLine())!=null) {
			if(s.contains("@"))
				titles.add(s);
			if(s.contains("#"))
				feas.add(s);
		}
		modelline[] res=new modelline[titles.size()];
		for(int i=0;i<titles.size();i++)
		{
			res[i]=new modelline();
			int numOfat=countChar(titles.get(i),'@');
			
			if(numOfat==1)
			{
				res[i].setIsRoot(true);
				res[i].setdepth(numOfat-1);
				String title=deleteChar(titles.get(i),'@');
				title=deleteChar(title,':');
				res[i].setname(title);
			}
			else
			{
				res[i].setdepth(numOfat-2);
				String title=deleteChar(titles.get(i),'@');
				String fea=deleteChar(feas.get(i),'#');
				fea=subString(fea, '(', ')');
				String[] temp_title=title.split(",");
				String[] temp_subtitle=temp_title[0].split(":");
				String[] temp_fea=fea.split(",");
				
				res[i].setfeature(temp_subtitle[0]);
				res[i].setfeaProbobility(Double.valueOf(temp_subtitle[1]));
				res[i].setname(deleteChar(temp_title[1],':'));
				for(int j=0;j<temp_fea.length;j++)
				{
					String[] temp_subfea=temp_fea[j].split(":");
					res[i].addlabel(temp_subfea[0]);
					res[i].addprobobilities(Double.valueOf(temp_subfea[1]));
				}
			}

		}
		return res;
	}
	private static int countChar(String s,char a) {
		int count=0;
		char[] ch=s.toCharArray();
		for(int i=0;i<ch.length;i++)
			if(ch[i]==a)
				count++;
		return count;
	}
	private static String deleteChar(String s,char a) {
		String res="";
		char[] ch=s.toCharArray();
		for(int i=0;i<ch.length;i++)
			if(ch[i]!=a)
				res=res+ch[i];
		return res;
	}
	private static String subString(String s,char a,char b) {
		String res="";
		boolean flag=false;
		char[] ch=s.toCharArray();
		for(int i=0;i<ch.length;i++)
		{
			if(ch[i]==b)
				flag=false;
			if(flag)
				res=res+ch[i];
			if(ch[i]==a)
				flag=true;			
		}				
		return res;
	}
    private static TreeNode readModel(modelline[] line){
		TreeNode root=new TreeNode();
		root.setName(line[0].getname());
	    if(!line[0].getIsRoot()) {
	    	root.setlabel(line[0].getlabel());
	    	root.setpropobilities(line[0].getprobobilities());
	    	root.setMaxlabel();
	    	root.setMaxprobobility();
	    }
		if(line.length==1)
		{
			root.setIsleaf(true);
			return root;
		}
		int depth=line[0].getdepth();
		ArrayList<Integer> fea_num=new ArrayList<Integer>();
		for(int i=1;i<line.length;i++)
			if(line[i].getdepth()==depth+1)
			{
				root.addFeatures(line[i].getfeature());
				root.addFeaProbobilities(line[i].getfeaProbobility());
				fea_num.add(i);
			}
		fea_num.add(line.length);
		for(int i=0;i<fea_num.size()-1;i++)
		{
			int hangshu=fea_num.get(i+1)-fea_num.get(i);
			modelline[] subline=new modelline[hangshu];
			for(int j=fea_num.get(i);j<fea_num.get(i+1);j++)
				subline[j-fea_num.get(i)]=line[j];
			root.addAttributes(readModel(subline));
		}
		return root;
    }
}
