package treeTools;

import java.util.ArrayList;

public class modelline {
	private int depth;
	private String feature;
	private double fea_probobility;
	private String name;
	private ArrayList<String> label=new ArrayList<String>();
	private ArrayList<Double> probobilities=new ArrayList<Double>();
	private boolean IsRoot=false;
	
	public void setdepth(int num) {
		depth=num;
	}
	public int getdepth() {
		return depth;
	}
	public void setfeature(String s) {
		feature=s;
	}
	public String getfeature() {
		return feature;
	}
	public void setfeaProbobility(double num) {
		fea_probobility=num;
	}
	public double getfeaProbobility() {
		return fea_probobility;
	}
	public void setname(String s) {
		name=s;
	}
	public String getname() {
		return name;
	}
	public void addlabel(String s) {
		label.add(s);
	}
	public ArrayList<String> getlabel(){
		return label;
	}
	public void addprobobilities(double num) {
		probobilities.add(num);
	}
	public ArrayList<Double> getprobobilities(){
		return probobilities;
	} 
	public void setIsRoot(boolean f) {
		IsRoot=f;
	}
	public boolean getIsRoot() {
		return IsRoot;
	}
}
