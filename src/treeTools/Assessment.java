package treeTools;

//import java.text.DecimalFormat;

public class Assessment {
	private static double accuracy;
	public static void exactACCtop1(int[] listReal,int[] listPre) {
		int count=0;
		for(int i=0;i<listReal.length;i++)
			if(listReal[i]==listPre[i])
				count++;
		accuracy=count/(listReal.length+0.0);
		//System.out.println("top1 acc:"+accuracy);
	}
	public static void fuzzACCtop1(double[][] listReal,double[][] listPre) {
		//DecimalFormat df = new DecimalFormat( "0.0000");
		double count=0;
		double score=1.0/(listReal[0].length-1);
		for(int i=0;i<listReal.length;i++)
		{
			int[] orderReal=sortIndex(listReal[i]);
			int[] orderPre=sortIndex(listPre[i]);
			int temp=orderPre.length-1;
			for(int j=orderPre.length-1;j>=0;j--)
			{
				if(orderReal[orderReal.length-1]==orderPre[j])
					break;
				temp--;
			}
			if(temp!=orderPre.length-1)
			{
				int temp_status=temp;
				while(temp>0)
				{
					if(listPre[i][orderPre[temp-1]]==listPre[i][orderPre[temp_status]])
						temp--;
					else
						break;
				}
			}
//			for(int j=0;j<listPre[i].length;j++)
//				System.out.print(df.format(listPre[i][j])+" ");
//			System.out.println(temp);
			count+=score*temp;
		}
		accuracy=count/listReal.length;
		//System.out.println("top1 acc:"+accuracy);
	}
	private static int[] sortIndex(double[] data) {
		int[] res=new int[data.length];
		for(int i=0;i<data.length;i++)
			res[i]=i;
		tools.mathtools.QuickSort(res,data,0,data.length-1);
		return res;
	}
	public static double getACC() {
		return accuracy;
	}
}
