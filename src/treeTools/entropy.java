package treeTools;

import java.util.ArrayList;
import java.util.Collections;

public class entropy {
	public static double getGain(double[][] attributes,double[][] label) {
		double res=0;
		double entropy=getentropy(label);
		double[] probobilities=getprobobility(attributes);
		int lieshu=probobilities.length;
		int hangshu=attributes.length;
		for(int i=0;i<lieshu;i++)
		{
			double[] feature=new double[hangshu];
			for(int j=0;j<hangshu;j++)
				feature[j]=attributes[j][i];
			double[][] fuzzCollect=getfuzzCollection(feature,label);
			double fea_entropy=getentropy(fuzzCollect);
			res+=probobilities[i]*fea_entropy;	
		}
		//System.out.print(entropy+" ");
		//System.out.print(res+" ");
		res=entropy-res;
		if(res<0.0)
			res=0;
		return res;
	}
	public static double[][] getfuzzCollection(double[] feature,double[][] label){
		double sum=tools.mathtools.countSum2D(label);
		int hangshu=label.length;
		int lieshu=label[0].length;
		double[][] res=new double[hangshu][lieshu];
		for(int i=0;i<hangshu;i++)
			for(int j=0;j<lieshu;j++)
				res[i][j]=label[i][j]*feature[i];
		double newSum=tools.mathtools.countSum2D(res);
		for(int i=0;i<hangshu;i++)
			for(int j=0;j<lieshu;j++)
			{
				res[i][j]=res[i][j]*sum/newSum;
				res[i][j]=res[i][j]>tools.Const.getAlpha()?res[i][j]:0;
			}
		return res;	
	}
	public static double[] getprobobility(double[][] data) {
		int hangshu=data.length;
		int lieshu=data[0].length;
		double[] probobilities=new double[lieshu];
		double sum=tools.mathtools.countSum2D(data);
		for(int i=0;i<lieshu;i++)
		{
			double[] newdata=new double[hangshu];
			for(int j=0;j<hangshu;j++)
				newdata[j]=data[j][i];
			double sum_temp=tools.mathtools.countSum(newdata);			
			probobilities[i]=sum_temp/sum;
		}
		return probobilities;
		
	}
	private static double getentropy(double[][] data) {
		double res=0;
		int hangshu=data.length;
		int lieshu=data[0].length;
		double[] probobilities=new double[lieshu];
		double sum=tools.mathtools.countSum2D(data);
		for(int i=0;i<lieshu;i++)
		{
			double[] newdata=new double[hangshu];
			for(int j=0;j<hangshu;j++)
				newdata[j]=data[j][i];
			double sum_temp=tools.mathtools.countSum(newdata);
			probobilities[i]=sum_temp/sum;
			if(probobilities[i]>0)
			res-=probobilities[i]*tools.mathtools.log2(probobilities[i]);
		}
		return res;
	}
	public static double getGain(int[] data,int[] label) {
		double res =0.0;
		double entropy=getentropy(label);
		ArrayList<Integer> features= getFeatures(data);
		double[] propobilities=getprobobility(data);
		for(int i=0;i<features.size();i++)
			res+=propobilities[i]*getentropy(getSubCollection(data,features.get(i),label));
		//System.out.print(entropy+" ");
		//System.out.print(res+" ");
		res=entropy-res;
		if(res<0.0)
			res=0;
		return res;
	}
	public static ArrayList<Integer> getFeatures(int[] data){
		ArrayList<Integer> label_class=new ArrayList<Integer>();
		for(int i=0;i<data.length;i++)
		{
			boolean flag=true;
			for(int j=0;j<label_class.size();j++)
				if(data[i]==label_class.get(j))				
					flag=false;
			if(flag)			
				label_class.add(data[i]);			
		}
		Collections.sort(label_class);
		return label_class;
	}
	public static int[] getSubCollection(int data[],int feature,int[] label) {
		ArrayList<Integer> num=new ArrayList<Integer>();
		for(int i=0;i<data.length;i++)
			if(data[i]==feature)
				num.add(label[i]);
		int[] res = new int[num.size()];
		for(int i=0;i<num.size();i++)
			res[i]=num.get(i);
		return res;
	}
	public static double[] getprobobility(int[] data){
		ArrayList<Integer> label_class=getFeatures(data);
		int[] label_count=new int[label_class.size()];
		
		for(int i=0;i<data.length;i++)
		{
			for(int j=0;j<label_class.size();j++)
				if(data[i]==label_class.get(j))
					label_count[j]++;
		}
		double[] res=new double[label_class.size()];
		int sum=data.length;
		for(int i=0;i<res.length;i++)
			res[i]=label_count[i]/(sum+0.0);
		return res;
	}
	private static double getentropy(int[] data) {
		double res=0.0;
		double[] probobilities=getprobobility(data);
		for(int i=0;i<probobilities.length;i++)
			if(probobilities[i]>0)
			res-=probobilities[i]*tools.mathtools.log2(probobilities[i]);		
		return res;
	}
}
